# Welcome to TuCSoNAndroid Repository

---

### What is TuCSoNAndroid?

TuCSoNAndroid (ToA for short) is an Android application suite enabling Android devices users/agents to exploit TuCSoN coordination services wrapped as an Android background service.

ToA is available under [GNU LGPL license](https://www.gnu.org/licenses/lgpl.html).