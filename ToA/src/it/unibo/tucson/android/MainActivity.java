/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.casestudy.CaseStudy;
import it.unibo.tucson.android.cli.Cli;
import it.unibo.tucson.android.geolocation.GeolocationFragment;
import it.unibo.tucson.android.introspection.Inspector;
import it.unibo.tucson.android.node.TucsonNode;
import it.unibo.tucson.android.settings.AboutFragment;
import it.unibo.tucson.android.settings.NotesFragment;
import it.unibo.tucson.android.settings.SettingsActivity;
import it.unibo.tucson.android.settings.SettingsFragment;
import it.unibo.tucson.android.tests.Tests;
import java.util.Locale;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class MainActivity extends FragmentActivity implements
ActionBar.TabListener {

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the primary sections of the app.
     */
    public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        /**
         *
         * @param fm
         *            the manager of Android fragments
         */
        public AppSectionsPagerAdapter(final FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public Fragment getItem(final int i) {
            switch (i) {
                case MainActivity.NODE:
                    return new TucsonNode();
                case MainActivity.CLI:
                    return new Cli();
                case MainActivity.INSPECTOR:
                    return new Inspector();
                case MainActivity.GEOLOCATION:
                    return new GeolocationFragment();
                case MainActivity.TEST:
                    return new Tests();
                case MainActivity.CASE_STUDY:
                    return new CaseStudy();
                default:
                    // The other sections of the app are dummy placeholders.
                    final Fragment fragment = new DummySectionFragment();
                    final Bundle args = new Bundle();
                    args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
                    fragment.setArguments(args);
                    return fragment;
            }
        }

        @Override
        public int getItemPosition(final Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(final int position) {
            // FIXME to check correctness of constant names
            switch (position) {
                case MainActivity.NODE:
                    return MainActivity.this.getString(R.string.title_section1)
                            .toUpperCase(Locale.getDefault());
                case MainActivity.CLI:
                    return MainActivity.this.getString(R.string.title_section2)
                            .toUpperCase(Locale.getDefault());
                case MainActivity.INSPECTOR:
                    return MainActivity.this.getString(R.string.title_section3)
                            .toUpperCase(Locale.getDefault());
                case MainActivity.GEOLOCATION:
                    return MainActivity.this.getString(R.string.title_section4)
                            .toUpperCase(Locale.getDefault());
                case MainActivity.TEST:
                    return MainActivity.this.getString(R.string.title_section5)
                            .toUpperCase(Locale.getDefault());
                case MainActivity.CASE_STUDY:
                    return MainActivity.this.getString(R.string.title_section6)
                            .toUpperCase(Locale.getDefault());
                default:
                    return "Section " + (position + 1);
            }
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply
     * displays dummy text.
     */
    public static class DummySectionFragment extends Fragment {

        /**
         *
         */
        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(final LayoutInflater inflater,
                final ViewGroup container, final Bundle savedInstanceState) {
            final View rootView = inflater.inflate(
                    R.layout.fragment_section_dummy, container, false);
            final Bundle args = this.getArguments();
            ((TextView) rootView.findViewById(android.R.id.text1)).setText(this
                    .getString(R.string.dummy_section_text, args
                            .getInt(DummySectionFragment.ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    private static final int CASE_STUDY = 5;
    private static final int CLI = 1;
    private static final int GEOLOCATION = 3;
    private static final int INSPECTOR = 2;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the three primary sections of the app. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private static final int NODE = 0;
    private static final int TEST = 4;
    /**
     * The {@link ViewPager} that will display the three primary sections of the
     * app, one at a time.
     */
    private ViewPager mViewPager;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        /*
         * Create the adapter that will return a fragment for each of the three
         * primary sections of the app
         */
        final AppSectionsPagerAdapter mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(
                this.getSupportFragmentManager());
        // Set up the action bar
        final ActionBar actionBar = this.getActionBar();
        /*
         * Specify that the Home/Up button should not be enabled, since there is
         * no hierarchical parent
         */
        actionBar.setHomeButtonEnabled(false);
        // Specify that we will be displaying tabs in the action bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        /*
         * Set up the ViewPager, attaching the adapter and setting up a listener
         * for when the user swipes between sections
         */
        this.mViewPager = (ViewPager) this.findViewById(R.id.pager);
        this.mViewPager.setAdapter(mAppSectionsPagerAdapter);
        this.mViewPager
        .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

                    @Override
            public void onPageSelected(final int position) {
                /*
                 * When swiping between different app sections, select
                 * the corresponding tab. We can also use
                 * ActionBar.Tab#select() to do this if we have a
                 * reference to the Tab.
                 */
                actionBar.setSelectedNavigationItem(position);
            }
        });
        // For each of the sections in the app, add a tab to the action bar
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            /*
             * Create a tab with text corresponding to the page title defined by
             * the adapter. Also specify this Activity object, which implements
             * the TabListener interface, as the listener for when this tab is
             * selected.
             */
            actionBar.addTab(actionBar.newTab()
                    .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                    .setTabListener(this));
        }
    }

    // Creates and adds icons to the item menu
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                /*
                 * This is in case we want to enable the main activity, for
                 * example if we wont to add a new fragment above the 3 tabs for
                 * help/about sections
                 */
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.startActivity(intent);
                return true;
            case R.id.menu_settings:
                // Starts settingsActivity
                intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.menu_about:
                /*
                 * Starts a fragment pop-up like to display about (i.e. tucson &
                 * 2p version, developer info, date of build and app version)
                 */
                final FragmentManager fm = this.getSupportFragmentManager();
                final AboutFragment about = new AboutFragment();
                about.show(fm, "about_fragment");
                return true;
            case R.id.menu_patchnotes:
                /*
                 * Starts a fragment pop-up like to display about (i.e. tucson &
                 * 2p version, developer info, date of build and app version)
                 */
                final FragmentManager fm2 = this.getSupportFragmentManager();
                final NotesFragment patchnotes = new NotesFragment();
                patchnotes.show(fm2, "patchnotes_fragment");
                return true;
            case R.id.menu_help:
                // Starts the browser, showing the Tucson Web Page
                final String url = "http://alice.unibo.it/xwiki/bin/view/TuCSoN/";
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabReselected(final ActionBar.Tab tab,
            final FragmentTransaction fragmentTransaction) {
        /*
         * Not used atm
         */
    }

    @Override
    public void onTabSelected(final ActionBar.Tab tab,
            final FragmentTransaction fragmentTransaction) {
        /*
         * When the given tab is selected, switch to the corresponding page in
         * the ViewPager.
         */
        this.mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(final ActionBar.Tab tab,
            final FragmentTransaction fragmentTransaction) {
        /*
         * Not used atm
         */
    }

    @Override
    protected void onDestroy() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this);
        sharedPref
        .edit()
        .putBoolean(SettingsFragment.getSettingsGeolocationExist(),
                false).commit();
        super.onDestroy();
    }
}
