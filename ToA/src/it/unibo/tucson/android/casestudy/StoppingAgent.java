/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.casestudy;

import it.unibo.tucson.android.settings.SettingsFragment;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 *
 */
public class StoppingAgent extends AbstractTucsonAgent {

    /**
     * Activity context
     */
    private final Context mContext;
    private final Handler messageHandler;
    /**
     * Case study tuple centre
     */
    private TucsonTupleCentreId tcId;

    /**
     *
     * @param id
     *            the ID of this TuCSoN agent
     * @param context
     *            the application context within which it's running
     * @param mh
     *            the message handler for this agent
     * @throws TucsonInvalidAgentIdException
     *             if the given String does not represent a valid TuCSoN agent
     *             identifier
     */
    public StoppingAgent(final String id, final Context context,
            final Handler mh) throws TucsonInvalidAgentIdException {
        super(id);
        this.mContext = context;
        this.messageHandler = mh;
        try {
            this.tcId = new TucsonTupleCentreId("default", "localhost",
                    String.valueOf(20504));
            this.checkTC();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError("TucsonInvalidTupleCentreIdException");
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used atm
         */
    }

    private void checkTC() throws TucsonInvalidTupleCentreIdException {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.mContext);
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsGeolocationPort(), "20504"));
        final String tuplecentre = sharedPref
                .getString(
                        SettingsFragment.getSettingsGeolocationTupleCentre(),
                        "default");
        if (!this.tcId.getName().equals(tuplecentre)
                || this.tcId.getPort() != port
                || !"localhost".equals(this.tcId.getNode())) {
            this.tcId = new TucsonTupleCentreId(tuplecentre, "localhost",
                    String.valueOf(port));
        }
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Stopping Agent]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postError(final String s) {
        this.say("Error: " + s);
        this.post("error", s);
    }

    @Override
    protected void main() {
        try {
            final SynchACC acc = this.getContext();
            acc.out(this.tcId, LogicTuple.parse("newPeer(stop)"), null);
            acc.out(this.tcId, LogicTuple.parse("currentPosition(stop,stop)"),
                    null);
        } catch (final InvalidLogicTupleException e) {
            this.postError("InvalidLogicTupleException");
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        }
    }
}
