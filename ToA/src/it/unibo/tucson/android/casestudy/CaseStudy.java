/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.casestudy;

import it.unibo.tucson.android.MainActivity;
import it.unibo.tucson.android.R;
import it.unibo.tucson.android.settings.SettingsFragment;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 *
 */
public class CaseStudy extends Fragment {

    /*****************************************************************************************
     * MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                CaseStudy.this.logTV.append(bundle.getString("who")
                        + " Error: " + bundle.getString("error") + "\n");
                CaseStudy.this.appendLog(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                CaseStudy.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                CaseStudy.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
            } else if (bundle.containsKey("cmd")) {
                if ("enableStart".equals(bundle.getString("cmd"))) {
                    CaseStudy.this.rootView.findViewById(R.id.buttonInit)
                    .setEnabled(false);
                    CaseStudy.this.buttonsStatus[0] = false;
                    CaseStudy.this.rootView.findViewById(R.id.buttonStart)
                    .setEnabled(true);
                    CaseStudy.this.buttonsStatus[1] = true;
                } else if ("enableStop".equals(bundle.getString("cmd"))) {
                    CaseStudy.this.rootView.findViewById(R.id.buttonStart)
                    .setEnabled(false);
                    CaseStudy.this.buttonsStatus[1] = false;
                    CaseStudy.this.rootView.findViewById(R.id.buttonStop)
                    .setEnabled(true);
                    CaseStudy.this.buttonsStatus[2] = true;
                } else if ("enableInit".equals(bundle.getString("cmd"))) {
                    CaseStudy.this.rootView.findViewById(R.id.buttonStop)
                    .setEnabled(false);
                    CaseStudy.this.buttonsStatus[2] = false;
                    CaseStudy.this.rootView.findViewById(R.id.buttonInit)
                    .setEnabled(true);
                    CaseStudy.this.buttonsStatus[0] = true;
                } else if ("showNotification".equals(bundle.getString("cmd"))) {
                    CaseStudy.this.showNotification(bundle
                            .getString("notification"));
                }
            }
            CaseStudy.this.performScrollDownOnLog();
        }
    }

    private static Context mContext;

    /**
     *
     * @return the application context
     */
    public static Context getContext() {
        return CaseStudy.mContext;
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void error(final String s) {
        System.out.println("[CaseStudy] Error: " + s);
    }

    private static boolean geolocationServiceExists() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(CaseStudy.mContext);
        return sharedPref.getBoolean(
                SettingsFragment.getSettingsGeolocationExist(), false);
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[CaseStudy] " + s);
    }

    private final boolean[] buttonsStatus = new boolean[] { true, false, false };
    private String log = "";
    private TextView logTV;
    private MessageHandler messageHandler;
    private View rootView;

    /**
     *
     * @param l
     *            the message to append to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout
        this.rootView = inflater.inflate(R.layout.fragment_casestudy,
                container, false);
        CaseStudy.mContext = this.getActivity();
        ((TextView) this.rootView.findViewById(R.id.frag_cs_description))
        .setText(Html.fromHtml(this.getString(R.string.cs_description)));
        this.logTV = (TextView) this.rootView.findViewById(R.id.frag_cs_log);
        // Init Button
        this.rootView.findViewById(R.id.buttonInit).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        CaseStudy.this.initTupleCentre();
                    }
                });
        // Start Button
        this.rootView.findViewById(R.id.buttonStart).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        CaseStudy.this.startCaseStudy();
                    }
                });
        // Stop Button
        this.rootView.findViewById(R.id.buttonStop).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        CaseStudy.this.stopCaseStudy();
                    }
                });
        this.messageHandler = new MessageHandler();
        return this.rootView;
    }

    @Override
    public void onResume() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        final String tuplecentre = sharedPref.getString(
                SettingsFragment.getSettingsCsServerTupleCentre(), "default");
        final String address = sharedPref.getString(
                SettingsFragment.getSettingsCsServerIpAddress(), "localhost");
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsCsServerPort(), "20504"));
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_cs_header);
        view.setText("Server on " + tuplecentre + "@" + address + ":" + port);
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        this.rootView.findViewById(R.id.buttonInit).setEnabled(
                this.buttonsStatus[0]);
        this.rootView.findViewById(R.id.buttonStart).setEnabled(
                this.buttonsStatus[1]);
        this.rootView.findViewById(R.id.buttonStop).setEnabled(
                this.buttonsStatus[2]);
        super.onResume();
    }

    /**
     *
     * @param notificationText
     *            the String to show as a notification
     */
    public void showNotification(final String notificationText) {
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                CaseStudy.mContext).setSmallIcon(R.drawable.tucson_icon)
                .setContentTitle("Case Study").setContentText(notificationText);
        final Intent resultIntent = new Intent(CaseStudy.mContext,
                MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // Because clicking the notification opens a new ("special") activity,
        // there's
        // no need to create an artificial back stack.
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                CaseStudy.mContext, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        final Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults = Notification.DEFAULT_ALL;
        final NotificationManager notificationManager = (NotificationManager) CaseStudy.mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

    /**
     * To start case study app execution
     */
    public void startCaseStudy() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        final String peerName = sharedPref.getString(
                SettingsFragment.getSettingsCsPeerName(), "caseStudyAgent");
        try {
            new CaseStudyAgent(peerName, CaseStudy.mContext,
                    this.messageHandler).go();
        } catch (final TucsonInvalidAgentIdException e) {
            this.postError("TucsonInvalidAgentIdException");
        }
    }

    /**
     * To stop case study app execution
     */
    public void stopCaseStudy() {
        try {
            new StoppingAgent("stoppingAgent", CaseStudy.mContext,
                    this.messageHandler).go();
        } catch (final TucsonInvalidAgentIdException e) {
            this.postError("TucsonInvalidAgentIdException");
        }
    }

    private void initTupleCentre() {
        if (!CaseStudy.geolocationServiceExists()) {
            this.postLog("No geolocation services found. Create one first!");
            Toast.makeText(this.getActivity(),
                    "No geolocation services found. Create one first!",
                    Toast.LENGTH_LONG).show();
        } else {
            try {
                new InitTupleCentreAgent("initCaseStudyAgent",
                        CaseStudy.mContext, this.messageHandler).go();
            } catch (final TucsonInvalidAgentIdException e) {
                this.postError("TucsonInvalidAgentIdException");
            }
        }
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_cs_log_sv).post(new Runnable() {

            @Override
            public void run() {
                ((ScrollView) CaseStudy.this.rootView
                        .findViewById(R.id.frag_cs_log_sv))
                        .fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void postError(final String s) {
        CaseStudy.error(s);
        this.logTV.append("[CaseStudy] Error: " + s + "\n");
        this.appendLog("[CaseStudy] Error: " + s + "\n");
        this.performScrollDownOnLog();
    }

    private void postLog(final String s) {
        CaseStudy.log(s);
        this.logTV.append("[CaseStudy] " + s + "\n");
        this.appendLog("[CaseStudy] " + s + "\n");
        this.performScrollDownOnLog();
    }
}
