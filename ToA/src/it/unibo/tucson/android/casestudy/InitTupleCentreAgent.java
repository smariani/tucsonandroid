/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.casestudy;

import it.unibo.tucson.android.settings.SettingsFragment;
import it.unibo.tucson.android.utils.Utils;
import java.io.IOException;
import java.io.InputStream;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 *
 */
public class InitTupleCentreAgent extends AbstractTucsonAgent {

    /**
     * ACC
     */
    private SynchACC acc;
    /**
     * Activity context
     */
    private final Context mContext;
    private final Handler messageHandler;
    /**
     * Case study tuple centre
     */
    private TucsonTupleCentreId tcId;

    /**
     *
     * @param id
     *            the ID of this TuCSoN agent
     * @param context
     *            the application context within which it's running
     * @param mh
     *            the message handler for this agent
     * @throws TucsonInvalidAgentIdException
     *             if the given String does not represent a valid TuCSoN agent
     *             identifier
     */
    public InitTupleCentreAgent(final String id, final Context context,
            final Handler mh) throws TucsonInvalidAgentIdException {
        super(id);
        this.mContext = context;
        this.messageHandler = mh;
        try {
            this.tcId = new TucsonTupleCentreId("default", "localhost",
                    String.valueOf(20504));
            this.checkTC();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError("TucsonInvalidTupleCentreIdException");
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used atm
         */
    }

    private void checkTC() throws TucsonInvalidTupleCentreIdException {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.mContext);
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsGeolocationPort(), "20504"));
        final String tuplecentre = sharedPref
                .getString(
                        SettingsFragment.getSettingsGeolocationTupleCentre(),
                        "default");
        if (!this.tcId.getName().equals(tuplecentre)
                || this.tcId.getPort() != port
                || !"localhost".equals(this.tcId.getNode())) {
            this.postLog("The tuple centre name and/or the port specified are not the same of the geolocation service. I'm doing that for you! :)");
            Toast.makeText(
                    this.mContext,
                    "The tuple centre name and/or the port specified are not the same of the geolocation service. I'm doing that for you! :)",
                    Toast.LENGTH_LONG).show();
            this.tcId = new TucsonTupleCentreId(tuplecentre, "localhost",
                    String.valueOf(port));
        }
    }

    private void injectSpecsInTC() throws IOException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'peer_spec' ReSpecT specification in tc < "
                + this.tcId.toString() + " >...");
        final InputStream is = this.mContext.getAssets().open("peer_spec.rsp");
        final LogicTuple specTuple = new LogicTuple("spec", new Value(
                Utils.fileToString(is)));
        this.acc.setS(this.tcId, specTuple, null);
    }

    private void insertTuples() {
        try {
            this.postLog("Initializing tuples in tc < " + this.tcId.toString()
                    + " >...");
            ITucsonOperation op = this.acc
                    .out(this.tcId,
                            LogicTuple
                            .parse("init(["
                                    + "orgPosition(coords(44.134029,12.058843),'office1'),"
                                    + // OFFICE1 --> ANDROID
                                    "orgPosition(coords(44.134025,12.058376),'office2'),"
                                    + // OFFICE2
                                    "orgPosition(coords(44.134196,12.058443),'office3')"
                                    + // OFFICE3
                                    "])"), null);
            if (!op.isResultSuccess()) {
                this.postError("problems during initialization");
            }
            op = this.acc.in(this.tcId, LogicTuple.parse("done"), null);
            if (op.isResultSuccess()) {
                this.postLog("Tuples initialized successfully");
            } else {
                this.postError("problems during initialization");
            }
        } catch (final InvalidLogicTupleException e) {
            this.postError("InvalidLogicTupleException");
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        }
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Init Agent]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postError(final String s) {
        this.say("Error: " + s);
        this.post("error", s);
    }

    private void postLog(final String s) {
        this.say(s);
        this.post("log", s);
    }

    private void sendCmd(final String cmd) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Init Agent]");
        bundle.putString("cmd", cmd);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    @Override
    protected void main() {
        try {
            this.acc = this.getContext();
            this.postLog("Initializing case study environment");
            this.injectSpecsInTC();
            this.insertTuples();
            this.postLog("Initialization completed");
            this.sendCmd("enableStart");
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        } catch (final IOException e) {
            this.postError("IOException");
        }
    }
}
