/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.casestudy;

import it.unibo.tucson.android.settings.SettingsFragment;
import java.util.List;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.logictuple.exceptions.InvalidTupleOperationException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.EnhancedACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tucson.network.NetworkUtils;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.util.Tools;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 *
 */
public class CaseStudyAgent extends AbstractTucsonAgent {

    /**
     * ACC
     */
    private EnhancedACC acc;
    private boolean interrupted;
    /**
     * Activity context
     */
    private final Context mContext;
    private final Handler messageHandler;
    private TucsonTupleCentreId server;
    /**
     * Case study tuple centre
     */
    private TucsonTupleCentreId tcId;

    /**
     *
     * @param id
     *            the ID of this TuCSoN agent
     * @param context
     *            the application context within which it's running
     * @param mh
     *            the message handler for this agent
     * @throws TucsonInvalidAgentIdException
     *             if the given String does not represent a valid TuCSoN agent
     *             identifier
     */
    public CaseStudyAgent(final String id, final Context context,
            final Handler mh) throws TucsonInvalidAgentIdException {
        super(id);
        this.mContext = context;
        this.messageHandler = mh;
        try {
            final SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(this.mContext);
            final String tuplecentre = sharedPref.getString(
                    SettingsFragment.getSettingsCsServerTupleCentre(),
                    "default");
            final String address = sharedPref.getString(
                    SettingsFragment.getSettingsCsServerIpAddress(),
                    "localhost");
            final int port = Integer.parseInt(sharedPref.getString(
                    SettingsFragment.getSettingsCsServerPort(), "20504"));
            this.server = new TucsonTupleCentreId(tuplecentre, address,
                    String.valueOf(port)); // peersLocationsTC
            this.tcId = new TucsonTupleCentreId("default", "localhost",
                    String.valueOf(20504));
            this.checkTC();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError("TucsonInvalidTupleCentreIdException");
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used atm
         */
    }

    private void checkTC() throws TucsonInvalidTupleCentreIdException {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.mContext);
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsGeolocationPort(), "20504"));
        final String tuplecentre = sharedPref
                .getString(
                        SettingsFragment.getSettingsGeolocationTupleCentre(),
                        "default");
        if (!this.tcId.getName().equals(tuplecentre)
                || this.tcId.getPort() != port
                || !"localhost".equals(this.tcId.getNode())) {
            this.tcId = new TucsonTupleCentreId(tuplecentre, "localhost",
                    String.valueOf(port));
        }
    }

    private void deleteGarbageTuples() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Removing garbage tuples..");
        this.acc.inAll(this.tcId, LogicTuple.parse("newPeer(X)"), (Long) null);
        this.acc.inAll(this.tcId, LogicTuple.parse("currentPosition(X,X)"),
                (Long) null);
        this.acc.inAll(this.tcId, LogicTuple.parse("currentOrgPosition(X)"),
                (Long) null);
        this.acc.in(this.tcId, LogicTuple.parse("orgPositionList(X)"),
                (Long) null);
        this.acc.in(this.tcId, LogicTuple.parse("nodePosition(S,X)"),
                (Long) null);
        this.postLog("Garbage tuples removed..");
    }

    // private String getOrgPos()
    // throws InvalidLogicTupleException, TucsonOperationNotPossibleException,
    // UnreachableNodeException, OperationTimeOutException,
    // InvalidTupleOperationException {
    //
    // this.postLog("Translating node ph position to org position...");
    // LogicTuple res = null;
    // List<Term> orgs = null;
    // ITucsonOperation op = this.acc.rd(this.tcId,
    // LogicTuple.parse("orgPositionList(OrgL)"), (Long) null);
    // if (op.isResultSuccess()) {
    // res = op.getLogicTupleResult();
    // orgs = res.getArg(0).toList();
    // } else {
    // this.postError("problems during ph -> org translation");
    // }
    // for (Term org : orgs) {
    // Struct currOrg = (Struct)org.getTerm();
    // String coords = currOrg.getArg(0).toString();
    // String orgPos = currOrg.getArg(1).toString();
    // op = this.acc.out(this.tcId, LogicTuple.parse("atOrgPos(" + coords +
    // ")"), (Long) null);
    // if (!op.isResultSuccess()) {
    // this.postError("problems during ph -> org translation");
    // }
    // op = this.acc.inp(this.tcId, LogicTuple.parse("atOrgPosRes(ok)"), (Long)
    // null);
    // if (op.isResultSuccess()) {
    // this.postLog("Translation completed!");
    // return orgPos;
    // } else {
    // this.postLog("The node is not at " + orgPos +
    // ", trying next known org position...");
    // this.acc.in(this.tcId, LogicTuple.parse("atOrgPos(" + coords + ")"),
    // (Long) null);
    // }
    // }
    //
    // this.postLog("The node is not at a known org position!");
    //
    // return "NA";
    // }
    private String getOrgPos() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException, InvalidTupleOperationException {
        final ITucsonOperation op = this.acc.rd(this.tcId,
                LogicTuple.parse("currentOrgPosition(Org)"), (Long) null);
        String orgPos = "";
        if (op.isResultSuccess()) {
            orgPos = op.getLogicTupleResult().getArg(0).toString();
            if ("NA".equals(orgPos)) {
                this.postLog("The node is not at a known org position!");
            } else {
                this.postLog("My current org position is: " + orgPos);
            }
        } else {
            this.postError("problems during current org position retrieval");
        }
        return orgPos;
    }

    private void notifyOrgPosToAll(final List<Term> peers)
            throws InvalidTupleOperationException, InvalidLogicTupleException,
            TucsonOperationNotPossibleException, UnreachableNodeException,
            OperationTimeOutException, TucsonInvalidTupleCentreIdException {
        final String orgPos = this.getOrgPos();
        for (final Term peer : peers) { // peer(Name, TcId, Ip, Port)
            final Struct currPeer = (Struct) peer.getTerm();
            if (!currPeer.getArg(0).toString().equals(this.myName())) {
                final TucsonTupleCentreId target = new TucsonTupleCentreId(
                        currPeer.getArg(1).toString(), currPeer.getArg(2)
                        .toString(), currPeer.getArg(3).toString());
                this.acc.out(
                        target,
                        LogicTuple.parse("currentPosition(" + this.myName()
                                + "," + orgPos + ")"), (Long) null);
            }
        }
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[CaseStudy Agent]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postError(final String s) {
        this.say("Error: " + s);
        this.post("error", s);
    }

    private void postLog(final String s) {
        this.say(s);
        this.post("log", s);
    }

    private void sendCmd(final String cmd) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[CaseStudy Agent]");
        bundle.putString("cmd", cmd);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void sendNotifyCmd(final String cmd, final String notification) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[CaseStudy Agent]");
        bundle.putString("cmd", cmd);
        bundle.putString("notification", notification);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    @Override
    protected void main() {
        try {
            this.acc = this.getContext();
            this.postLog("Waiting for connect command...");
            ITucsonOperation op = this.acc.in(this.tcId,
                    LogicTuple.parse("connect"), (Long) null);
            if (op.isResultSuccess()) {
                this.postLog("Connect command received");
            } else {
                this.postError("problems during connection");
            }
            // reaction(
            // out(checkOrgPosition([orgPosition(Pos,Org)|Orgs])),
            // (internal, near(ph,Pos,2)),
            // (
            // nop(connect),
            // out(connect)
            // )
            // ).
            this.acc.inS(
                    this.tcId,
                    LogicTuple
                    .parse("out(checkOrgPosition([orgPosition(Pos,Org)|Orgs]))"),
                    LogicTuple.parse("( internal, near(ph,Pos,2) )"),
                    LogicTuple.parse("( nop(connect), out(connect) )"),
                    (Long) null);
            this.postLog("Retrieving my node ip position...");
            op = this.acc.rd(this.tcId, LogicTuple.parse("nodePosition(ip,P)"),
                    (Long) null);
            String nodeIp = "";
            if (op.isResultSuccess()) {
                final LogicTuple res = op.getLogicTupleResult();
                nodeIp = NetworkUtils.getIp(Tools.removeApices(res.getArg(1)
                        .toString()));
            } else {
                this.postError("problems during node ip position retrieval");
            }
            this.postLog("Inserting my position in server tc < "
                    + this.server.toString() + " >...");
            this.acc.out(
                    this.server,
                    LogicTuple.parse("peer(" + this.myName() + ","
                            + this.tcId.getName() + "," + "'" + nodeIp + "'"
                            + "," + this.tcId.getPort() + ")"), (Long) null);
            LogicTuple res = null;
            this.postLog("Getting peers...");
            op = this.acc.rd(this.server, LogicTuple.parse("neighbours(Nbrs)"),
                    (Long) null);
            res = op.getLogicTupleResult();
            final List<Term> peers = res.getArg(0).toList();
            if (!peers.isEmpty()
                    && !(peers.size() == 1 && ((Struct) peers.get(0).getTerm())
                    .getArg(0).toString().equals(this.myName()))) {
                this.postLog("Notifing my organisational position to others...");
                this.notifyOrgPosToAll(peers);
            } else {
                this.postLog("There are no peers...");
            }
            this.sendCmd("enableStop");
            while (!this.interrupted) {
                this.postLog("Waiting for new neighbours...");
                op = this.acc.in(this.tcId, LogicTuple.parse("newPeer(N)"),
                        (Long) null);
                if (op.isResultSuccess()) {
                    res = op.getLogicTupleResult();
                } else {
                    this.postError("problems during new peer retrieval");
                    break;
                }
                if (!"stop".equals(res.getArg(0).toString())) {
                    this.postLog("New neighbour is arrived: " + res.getArg(0));
                } else {
                    this.interrupted = true;
                    break;
                }
                this.postLog("Waiting for organisational position from the new neighbour...");
                op = this.acc.in(this.tcId,
                        LogicTuple.parse("currentPosition(Name,OrgPos)"),
                        (Long) null);
                if (op.isResultSuccess()) {
                    res = op.getLogicTupleResult();
                } else {
                    this.postError("problems during organisational position retrieval");
                    break;
                }
                if (!"stop".equals(res.getArg(0).toString())) {
                    this.postLog("The new neighbour " + res.getArg(0)
                            + " is at " + res.getArg(1));
                    this.sendNotifyCmd("showNotification", res.getArg(0)
                            .toString()
                            + " is arrived at "
                            + res.getArg(1).toString() + "!");
                } else {
                    this.interrupted = true;
                    break;
                }
            }
            this.postLog("Stopping..");
            this.postLog("Unregistering from server tc < "
                    + this.server.toString() + " >...");
            this.acc.in(
                    this.server,
                    LogicTuple.parse("peer(" + this.myName() + ","
                            + this.tcId.getName() + "," + "'" + nodeIp + "'"
                            + "," + this.tcId.getPort() + ")"), (Long) null);
            this.deleteGarbageTuples();
            this.sendCmd("enableInit");
            this.postLog("Stopped");
        } catch (final InvalidLogicTupleException e) {
            this.postError("InvalidLogicTupleException");
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        } catch (final InvalidTupleOperationException e) {
            this.postError("InvalidTupleOperationException");
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError("TucsonInvalidTupleCentreIdException");
        }
    }
}
