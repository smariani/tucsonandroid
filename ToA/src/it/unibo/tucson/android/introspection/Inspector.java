/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.introspection;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.settings.SettingsFragment;
import java.io.IOException;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.introspection.InspectorContext;
import alice.tucson.introspection.InspectorProtocol;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 * @author (contributor) Roberto D'Elia (mailto: roberto.delia3@studio.unibo.it)
 *
 */
public class Inspector extends Fragment {

    /*****************************************************************************************
     * MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("tuples")) {
                Inspector.this.tuplesTV.setText(bundle.getString("tuples"));
                Inspector.this.setTuples(bundle.getString("tuples"));
                Inspector.this.performScrollDownOnTuples();
            } else if (bundle.containsKey("events")) {
                Inspector.this.eventsTV.setText(bundle.getString("events"));
                Inspector.this.setEvents(bundle.getString("events"));
                Inspector.this.performScrollDownOnEvents();
            } else if (bundle.containsKey("reaction")) {
                Inspector.this.reactionsTV.append(bundle.getString("reaction")
                        + "\n");
                Inspector.this.appendReaction(bundle.getString("reaction")
                        + "\n");
                Inspector.this.performScrollDownOnReactions();
            } else if (bundle.containsKey("error")) {
                Inspector.this.logTV.append(bundle.getString("who")
                        + " Error: " + bundle.getString("error") + "\n");
                Inspector.this.appendLog(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
                Inspector.this.performScrollDownOnLog();
            } else if (bundle.containsKey("log")) {
                Inspector.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                Inspector.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                Inspector.this.performScrollDownOnLog();
            } else if (bundle.containsKey("clear")) {
                Inspector.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("clear") + "\n");
                Inspector.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("clear") + "\n");
                Inspector.this.performScrollDownOnLog();
                Inspector.this.clearViews();
                Inspector.this.logTV.append(bundle.getString("who")
                        + " Views cleared\n");
                Inspector.this.appendLog(bundle.getString("who")
                        + " Views cleared\n");
                Inspector.this.performScrollDownOnLog();
                Inspector.this.spinner.setSelection(0, true);
            }
        }
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Inspector] " + s);
    }

    private TucsonAgentId aid;
    private InspectorContext context;
    private String events = "", reactions = "", tuples = "", log = "";
    private TextView eventsTV;
    private InspectorCore inspector;
    private TextView logTV;
    private MessageHandler messageHandler;
    private String node;
    private int port;
    private TextView reactionsTV;
    private View rootView;
    private Spinner spinner;
    private TucsonTupleCentreId tid;
    private String tuplecenter;
    private TextView tuplesTV;
    /**
     * The communication protocol used by the Inspector
     */
    protected InspectorProtocol protocol = new InspectorProtocol();

    /**
     *
     * @param l
     *            the message to appen to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    /**
     *
     * @param reaction
     *            the String representation of the ReSpecT reaction to append to
     *            the list of reactions
     */
    public void appendReaction(final String reaction) {
        this.reactions = (this.reactions.equals("") ? "" : this.reactions)
                + reaction;
    }

    /**
     * To clear all the views text
     */
    public void clearViews() {
        this.tuplesTV.setText("");
        this.tuples = "";
        this.eventsTV.setText("");
        this.events = "";
        this.reactionsTV.setText("");
        this.reactions = "";
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout
        this.rootView = inflater.inflate(R.layout.fragment_inspector,
                container, false);
        final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
        .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        this.rootView.findViewById(R.id.frag_ispector_start)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                Inspector.this.startInspection();
            }
        });
        this.rootView.findViewById(R.id.frag_ispector_stop).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        Inspector.this.stopInspection();
                    }
                });
        this.logTV = (TextView) this.rootView
                .findViewById(R.id.frag_inspector_log);
        this.tuplesTV = (TextView) this.rootView
                .findViewById(R.id.frag_inspector_tuples);
        this.eventsTV = (TextView) this.rootView
                .findViewById(R.id.frag_inspector_events);
        this.reactionsTV = (TextView) this.rootView
                .findViewById(R.id.frag_inspector_reactions);
        this.spinner = (Spinner) this.rootView
                .findViewById(R.id.inspector_spinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(this.getActivity(),
                        R.array.inspector_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner.setAdapter(adapter);
        this.spinner
        .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
            public void onItemSelected(final AdapterView<?> parent,
                    final View view, final int pos, final long id) {
                final String sel = parent.getItemAtPosition(pos)
                        .toString();
                if ("Tuples".equals(sel)) {
                    ((ScrollView) Inspector.this.tuplesTV.getParent())
                    .setVisibility(View.VISIBLE);
                    ((ScrollView) Inspector.this.eventsTV.getParent())
                    .setVisibility(View.GONE);
                    ((ScrollView) Inspector.this.reactionsTV
                            .getParent()).setVisibility(View.GONE);
                } else if ("Events".equals(sel)) {
                    ((ScrollView) Inspector.this.tuplesTV.getParent())
                    .setVisibility(View.GONE);
                    ((ScrollView) Inspector.this.eventsTV.getParent())
                    .setVisibility(View.VISIBLE);
                    ((ScrollView) Inspector.this.reactionsTV
                            .getParent()).setVisibility(View.GONE);
                } else {
                    ((ScrollView) Inspector.this.tuplesTV.getParent())
                    .setVisibility(View.GONE);
                    ((ScrollView) Inspector.this.eventsTV.getParent())
                    .setVisibility(View.GONE);
                    ((ScrollView) Inspector.this.reactionsTV
                            .getParent()).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
                /*
                 * Not used atm
                 */
            }
        });
        this.messageHandler = new MessageHandler();
        return this.rootView;
    }

    @Override
    public void onResume() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        this.tuplecenter = sharedPref.getString(
                SettingsFragment.getSettingsInspectorTupleCentre(), "default");
        this.node = sharedPref.getString(
                SettingsFragment.getSettingsInspectorIpAddress(), "localhost");
        this.port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsInspectorPort(), "20504"));
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_inspector_header);
        view.setText("Inspecting " + this.tuplecenter + "@" + this.node + ":"
                + this.port);
        this.tuplesTV.setText(this.tuples);
        this.eventsTV.setText(this.events);
        this.reactionsTV.setText(this.reactions);
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        super.onResume();
    }

    /**
     *
     * @param e
     *            the String representation of the events set to set
     */
    public void setEvents(final String e) {
        this.events = e;
    }

    /**
     *
     * @param t
     *            the String representation of the tuples set to set
     */
    public void setTuples(final String t) {
        this.tuples = t;
    }

    private void performScrollDownOnEvents() {
        this.rootView.findViewById(R.id.frag_inspector_events_sv).post(
                new Runnable() {

                    @Override
                    public void run() {
                        ((ScrollView) Inspector.this.rootView
                                .findViewById(R.id.frag_inspector_events_sv))
                                .fullScroll(View.FOCUS_DOWN);
                    }
                });
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_inspector_log_sv).post(
                new Runnable() {

                    @Override
                    public void run() {
                        ((ScrollView) Inspector.this.rootView
                                .findViewById(R.id.frag_inspector_log_sv))
                                .fullScroll(View.FOCUS_DOWN);
                    }
                });
    }

    private void performScrollDownOnReactions() {
        this.rootView.findViewById(R.id.frag_inspector_reactions_sv).post(
                new Runnable() {

                    @Override
                    public void run() {
                        ((ScrollView) Inspector.this.rootView
                                .findViewById(R.id.frag_inspector_reactions_sv))
                                .fullScroll(View.FOCUS_DOWN);
                    }
                });
    }

    private void performScrollDownOnTuples() {
        this.rootView.findViewById(R.id.frag_inspector_tuples_sv).post(
                new Runnable() {

                    @Override
                    public void run() {
                        ((ScrollView) Inspector.this.rootView
                                .findViewById(R.id.frag_inspector_tuples_sv))
                                .fullScroll(View.FOCUS_DOWN);
                    }
                });
    }

    private void postLog(final String s) {
        Inspector.log(s);
        this.logTV.append("[Inspector] " + s + "\n");
        this.appendLog("[Inspector] " + s + "\n");
        this.performScrollDownOnLog();
    }

    private void startInspection() {
        if (this.inspector == null || this.inspector != null
                && !this.inspector.isRunning()) {
            final SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(this.getActivity());
            this.tuplecenter = sharedPref.getString(
                    SettingsFragment.getSettingsInspectorTupleCentre(),
                    "default");
            this.node = sharedPref.getString(
                    SettingsFragment.getSettingsInspectorIpAddress(),
                    "localhost");
            this.port = Integer.parseInt(sharedPref.getString(
                    SettingsFragment.getSettingsInspectorPort(), "20504"));
            try {
                this.aid = new TucsonAgentId("inspector"
                        + System.currentTimeMillis());
                this.tid = new TucsonTupleCentreId(this.tuplecenter, this.node,
                        String.valueOf(this.port));
            } catch (final TucsonInvalidAgentIdException e) {
                e.printStackTrace();
            } catch (final TucsonInvalidTupleCentreIdException e) {
                e.printStackTrace();
            }
            this.inspector = new InspectorCore(this.aid, this.tid,
                    this.messageHandler);
            this.context = this.inspector.getContext();
            this.protocol
            .setTsetObservType(InspectorProtocol.PROACTIVE_OBSERVATION);
            this.protocol
            .setPendingQueryObservType(InspectorProtocol.PROACTIVE_OBSERVATION);
            this.protocol
            .setReactionsObservType(InspectorProtocol.PROACTIVE_OBSERVATION);
            try {
                this.context.setProtocol(this.protocol);
            } catch (final IOException e) {
                e.printStackTrace();
            }
            this.inspector.execute();
        } else {
            this.postLog("Inspector already started");
            Toast.makeText(this.getActivity(), "Inspector already started",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void stopInspection() {
        if (this.inspector != null && this.inspector.isRunning()) {
            this.protocol.setTsetObservType(InspectorProtocol.NO_OBSERVATION);
            this.protocol
            .setPendingQueryObservType(InspectorProtocol.NO_OBSERVATION);
            this.protocol
            .setReactionsObservType(InspectorProtocol.NO_OBSERVATION);
            try {
                this.context.setProtocol(this.protocol);
            } catch (final IOException e) {
                e.printStackTrace();
            }
            this.inspector.quit();
        } else {
            this.postLog("Inspector not started yet");
            Toast.makeText(this.getActivity(), "Inspector  not started yet",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
