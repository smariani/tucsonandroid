/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.introspection;

import java.util.Calendar;
import java.util.Iterator;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.introspection.InspectorContextEvent;
import alice.tucson.introspection.WSetEvent;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.core.TriggeredReaction;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 * @author (contributor) Roberto D'Elia (mailto: roberto.delia3@studio.unibo.it)
 *
 */
public class InspectorCore extends InspectorAsynchTask {

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Inspector] " + s);
    }

    private final Handler messageHandler;

    /**
     *
     * @param id
     *            the TuCSoN agent ID given to this Inspector instance
     * @param tid
     *            the TuCSoN tuple centre ID of the tuple centre this Inspector
     *            should inspect
     * @param mh
     *            the messahe handler for this component
     */
    public InspectorCore(final TucsonAgentId id, final TucsonTupleCentreId tid,
            final Handler mh) {
        super(id, tid, mh);
        this.messageHandler = mh;
        this.postLog("Core created");
    }

    @Override
    public void onContextEvent(final InspectorContextEvent msg) {
        this.postLog("Context event received");
        Calendar cal = Calendar.getInstance();
        // Tuples
        if (msg.getTuples() != null) {
            final StringBuffer st = new StringBuffer();
            final Iterator<? extends Tuple> it = msg.getTuples().iterator();
            int n = 0;
            while (it.hasNext()) {
                st.append(it.next().toString()).append('\n');
                n++;
            }
            this.postTuples(st.toString());
            String nS = "are " + n + " tuples";
            if (n == 0) {
                nS = "are no tuples";
            } else if (n == 1) {
                nS = "is " + n + " tuple";
            }
            this.postLog("Tuples has been updated. There " + nS + ".");
        }
        // Events
        if (msg.getWnEvents() != null) {
            final StringBuffer st = new StringBuffer();
            final Iterator<WSetEvent> it = msg.getWnEvents().iterator();
            int n = 0;
            WSetEvent ev;
            while (it.hasNext()) {
                ev = it.next();
                st.append(ev.getOp())
                .append(" from <")
                .append(((TucsonAgentId) ev.getSource()).getAgentName())
                .append("> to <").append(ev.getTarget()).append(">\n");
                n++;
            }
            this.postEvents(st.toString());
            String nS = "are " + n + " events";
            if (n == 0) {
                nS = "are no events";
            } else if (n == 1) {
                nS = "is " + n + " event";
            }
            this.postLog("Events has been updated. There " + nS + ".");
        }
        // Reactions
        if (msg.getReactionOk() != null) {
            cal = Calendar.getInstance();
            final TriggeredReaction tr = msg.getReactionOk();
            this.postReaction("reaction < " + tr.getReaction()
                    + " > SUCCEEDED @ " + cal.get(Calendar.HOUR_OF_DAY) + ":"
                    + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND)
                    + ".\n");
            this.postLog("Reactions has been updated");
        } else if (msg.getReactionFailed() != null) {
            cal = Calendar.getInstance();
            final TriggeredReaction tr = msg.getReactionFailed();
            this.postReaction("reaction < " + tr.getReaction() + " > FAILED @ "
                    + cal.get(Calendar.HOUR_OF_DAY) + ":"
                    + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND)
                    + ".\n");
            this.postLog("Reaction triggered.");
        }
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Inspector]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postEvents(final String s) {
        this.post("events", s);
    }

    private void postLog(final String s) {
        InspectorCore.log(s);
        this.post("log", s);
    }

    private void postReaction(final String s) {
        this.post("reaction", s);
    }

    private void postTuples(final String s) {
        this.post("tuples", s);
    }
}
