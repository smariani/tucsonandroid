/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.introspection;

import java.io.IOException;
import alice.tucson.api.TucsonAgentId;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.introspection.InspectorContext;
import alice.tucson.introspection.InspectorContextEvent;
import alice.tucson.introspection.InspectorContextListener;
import alice.tucson.introspection.InspectorContextStub;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 * @author (contributor) Roberto D'Elia (mailto: roberto.delia3@studio.unibo.it)
 *
 */
public class InspectorAsynchTask extends AsyncTask<String, Void, String>
implements InspectorContextListener {

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Inspector] " + s);
    }

    private final Handler messageHandler;
    private boolean running;
    /**
     *
     */
    protected InspectorContext context;
    /**
     *
     */
    protected boolean q;

    /**
     *
     * @param id
     *            the agent identifier this inspector should use
     * @param tid
     *            the identifier of the tuple centre under inspection
     * @param mh
     *            the message handler for this app
     */
    public InspectorAsynchTask(final TucsonAgentId id,
            final TucsonTupleCentreId tid, final Handler mh) {
        super();
        this.context = new InspectorContextStub(id, tid);
        this.context.addInspectorContextListener(this);
        this.q = false;
        this.running = false;
        this.messageHandler = mh;
    }

    /**
     *
     * @return the inspection context used by this inspector
     */
    public InspectorContext getContext() {
        return this.context;
    }

    /**
     *
     * @return wether this task is running
     */
    public boolean isRunning() {
        return this.running;
    }

    /**
     * Context event callback, not used atm
     *
     * @param ev
     *            the context event to handle
     */
    @Override
    public void onContextEvent(final InspectorContextEvent ev) {
        /*
         * Not used atm
         */
    }

    /**
     *
     */
    public void quit() {
        this.q = true;
        this.running = false;
        this.postClear("Clearing all views");
        try {
            this.context.exit();
        } catch (final IOException e) {
            this.postError(e.toString());
        }
        this.cancel(true);
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Inspector]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postClear(final String s) {
        InspectorAsynchTask.log(s);
        this.post("clear", s);
    }

    private void postError(final String s) {
        InspectorAsynchTask.log("Error: " + s);
        this.post("error", s);
    }

    private void postLog(final String s) {
        InspectorAsynchTask.log(s);
        this.post("log", s);
    }

    @Override
    protected String doInBackground(final String... arg0) {
        this.postLog("Started inspecting TuCSoN Node < "
                + this.context.getTid().getName() + "@"
                + this.context.getTid().getNode() + ":"
                + this.context.getTid().getPort() + " >");
        this.running = true;
        while (!this.isCancelled()) {
            try {
                this.context.acceptVMEvent();
            } catch (final ClassNotFoundException e) {
                this.postError(e.toString());
                break;
            } catch (final IOException e) {
                this.postError(e.toString());
                break;
            }
        }
        this.postLog("Stopped inspecting TuCSoN Node < "
                + this.context.getTid().getName() + "@"
                + this.context.getTid().getNode() + ":"
                + this.context.getTid().getPort() + " >");
        return null;
    }
}
