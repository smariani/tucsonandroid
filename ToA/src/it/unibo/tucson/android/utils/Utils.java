/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
/**
 * This work by Danilo Pianini is licensed under a Creative Commons
 * Attribution-NonCommercial-ShareAlike 3.0 Italy License. Permissions beyond
 * the scope of this license may be available at www.danilopianini.org.
 */
package it.unibo.tucson.android.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Credits go to the author below.
 *
 * @author Danilo Pianini
 * @author (contributor) ste (mailto: s.mariani@unibo.it)
 * @author (contributor) Michele Bombardi (mailto:
 *         michele.bombardi@studio.unibo.it)
 *
 */
public final class Utils {

    /**
     *
     * @param is
     *            the input stream opened on the file to read
     * @return the String representation of the content of the read file
     * @throws IOException
     *             if the file cannot be found or access permissions do not
     *             allow reading
     */
    public static String fileToString(final InputStream is) throws IOException {
        final BufferedInputStream br = new BufferedInputStream(is);
        final byte[] res = new byte[br.available()];
        br.read(res);
        br.close();
        return new String(res);
    }

    /**
     *
     * @param path
     *            the filepath toward the file to be read
     * @return the String representation of the content of the read file
     * @throws IOException
     *             if the file cannot be found or access permissions do not
     *             allow reading
     */
    public static String fileToString(final String path) throws IOException {
        final BufferedInputStream br = new BufferedInputStream(
                new FileInputStream(path));
        final byte[] res = new byte[br.available()];
        br.read(res);
        br.close();
        return new String(res);
    }

    private Utils() {
        /*
         *
         */
    }
}
