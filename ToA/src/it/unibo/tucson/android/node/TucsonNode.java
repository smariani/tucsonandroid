/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.node;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.node.service.NodeService;
import it.unibo.tucson.android.settings.SettingsFragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class TucsonNode extends Fragment {

    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                TucsonNode.this.statusTV.setText("Error");
                TucsonNode.this.logTV.append(bundle.getString("who")
                        + " Error: " + bundle.getString("error") + "\n");
                TucsonNode.this.appendLog(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                final SharedPreferences sharedPref = PreferenceManager
                        .getDefaultSharedPreferences(TucsonNode.this.appContext);
                final int port = Integer.parseInt(sharedPref.getString(
                        SettingsFragment.getSettingsNodePort(), "20504"));
                TucsonNode.this.statusTV.setText(bundle.getString("log")
                        + " on port " + port);
                TucsonNode.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                TucsonNode.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
            }
            TucsonNode.this.performScrollDownOnLog();
        }

        public void postError(final String who, final String error) {
            final Message msg = this.obtainMessage();
            final Bundle b = new Bundle();
            b.putString("who", who);
            b.putString("error", error);
            msg.setData(b);
            this.sendMessage(msg);
        }

        public void postLog(final String who, final String l) {
            final Message msg = this.obtainMessage();
            final Bundle b = new Bundle();
            b.putString("who", who);
            b.putString("log", l);
            msg.setData(b);
            this.sendMessage(msg);
        }
    }

    /*****************************************************************************************
     * BROADCAST RECEIVER AND MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    private class NodeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("tucson.node.LOG".equalsIgnoreCase(action)) {
                TucsonNode.this.messageHandler.postLog(
                        intent.getStringExtra("who"),
                        intent.getStringExtra("msg"));
            } else if ("tucson.node.ERROR".equalsIgnoreCase(action)) {
                TucsonNode.this.messageHandler.postError(
                        intent.getStringExtra("who"),
                        intent.getStringExtra("msg"));
            }
        }
    }

    private static Context mContext;

    /**
     *
     * @return the application context the TuCSoN Node is running into
     */
    public static Context getContext() {
        return TucsonNode.mContext;
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Tucson Node] " + s);
    }

    private Context appContext;
    private NodeReceiver broadcastReceiver;
    private String log = "";
    private TextView logTV;
    /*****************************************************************************************
     * SERVICE BINDING
     *****************************************************************************************/
    private NodeService mBoundService;
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(final ComponentName className,
                final IBinder service) {
            /*
             * This is called when the connection with the service has been
             * established, giving us the service object we can use to interact
             * with the service. Because we have bound to a explicit service
             * that we know is running in our own process, we can cast its
             * IBinder to a concrete class and directly access it.
             */
            TucsonNode.this.mBoundService = ((NodeService.NodeServiceBinder) service)
                    .getService();
            // mBoundService.setContext(((MainActivity) mContext).getContext());
        }

        @Override
        public void onServiceDisconnected(final ComponentName className) {
            /*
             * This is called when the connection with the service has been
             * unexpectedly disconnected -- that is, its process crashed.
             * Because it is running in our same process, we should never see
             * this happen.
             */
            TucsonNode.this.mBoundService = null;
        }
    };
    private MessageHandler messageHandler;
    private boolean mIsBound;
    private View rootView;
    private TextView statusTV;

    /**
     *
     * @param l
     *            the String to append to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout
        this.rootView = inflater.inflate(R.layout.fragment_node, container,
                false);
        this.appContext = this.getActivity().getApplicationContext();
        TucsonNode.mContext = this.getActivity();
        if (this.messageHandler == null) {
            this.messageHandler = new MessageHandler();
        }
        if (this.broadcastReceiver == null) {
            this.broadcastReceiver = new NodeReceiver();
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.node.LOG");
        intentFilter.addAction("tucson.node.ERROR");
        TucsonNode.mContext.registerReceiver(this.broadcastReceiver,
                intentFilter);
        this.statusTV = (TextView) this.rootView
                .findViewById(R.id.frag_node_text);
        this.logTV = (TextView) this.rootView.findViewById(R.id.frag_node_log);
        // StartService Button
        this.rootView.findViewById(R.id.buttonStartService).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        TucsonNode.this.doBindService();
                        TucsonNode.this.startNodeService();
                    }
                });
        // StopService Button
        this.rootView.findViewById(R.id.buttonStopService).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(final View view) {
                        TucsonNode.this.stopNodeService();
                    }
                });
        return this.rootView;
    }

    // This unbinds the service when the activity has been killed somehow
    @Override
    public void onDestroy() {
        if (this.serviceIsRunning()) {
            this.stopNodeService();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        super.onResume();
    }

    @Override
    public void onStart() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.node.LOG");
        intentFilter.addAction("tucson.node.ERROR");
        TucsonNode.mContext.registerReceiver(this.broadcastReceiver,
                intentFilter);
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsNodePort(), "20504"));
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_node_header);
        view.setText("Executing on default@localhost:" + port);
        super.onStart();
    }

    /*****************************************************************************************/
    @Override
    public void onStop() {
        TucsonNode.mContext.unregisterReceiver(this.broadcastReceiver);
        super.onStop();
    }

    /**
     *
     * @return wether the TuCSoN Node service is running
     */
    public boolean serviceIsRunning() {
        return this.mBoundService != null && this.mBoundService.isRunning();
    }

    /**
     * To stop TuCSoN Node service
     */
    public void stopNodeService() {
        if (this.serviceIsRunning()) {
            this.doUnbindService();
            this.postLog("Stopping TuCSoN node service");
            TucsonNode.mContext.stopService(new Intent(this.getActivity(),
                    NodeService.class));
        } else {
            this.postLog("Tucson Node not started yet");
            Toast.makeText(TucsonNode.mContext, "Tucson Node not started yet",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void doBindService() {
        /*
         * Establish a connection with the service. We use an explicit class
         * name because we want a specific service implementation that we know
         * will be running in our own process (and thus won't be supporting
         * component replacement by other applications).
         */
        TucsonNode.mContext.bindService(new Intent(this.getActivity(),
                NodeService.class), this.mConnection, Context.BIND_AUTO_CREATE);
        this.mIsBound = true;
    }

    private void doUnbindService() {
        if (this.mIsBound) {
            // Detach our existing connection.
            TucsonNode.mContext.unbindService(this.mConnection);
            this.mIsBound = false;
        }
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_node_log_sv).post(new Runnable() {

            @Override
            public void run() {
                ((ScrollView) TucsonNode.this.rootView
                        .findViewById(R.id.frag_node_log_sv))
                        .fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void postLog(final String s) {
        TucsonNode.log(s);
        this.logTV.append("[Tucson Node] " + s + "\n");
        this.appendLog("[Tucson Node] " + s + "\n");
        this.performScrollDownOnLog();
    }

    private void startNodeService() {
        final Intent intent = new Intent(this.getActivity(), NodeService.class);
        if (!this.serviceIsRunning()) {
            this.postLog("Starting TuCSoN node service with intent "
                    + intent.toString());
            TucsonNode.mContext.startService(intent);
        } else {
            this.postLog("Node already started");
            Toast.makeText(TucsonNode.mContext, "Tucson Node already started",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
