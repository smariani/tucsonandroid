/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.node.service;

import it.unibo.tucson.android.R;
import alice.tucson.service.TucsonNodeService;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class NodeStopAsyncTask extends AsyncTask<String, Void, String> {

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[StopAsyncTask] " + s);
    }

    private final TucsonNodeService node;
    private final NodeService nodeService;

    /**
     *
     * @param ns
     *            the node service istance to stop
     * @param n
     *            the correspondant TuCSoN Node service
     */
    public NodeStopAsyncTask(final NodeService ns, final TucsonNodeService n) {
        super();
        this.node = n;
        this.nodeService = ns;
    }

    private void sendBroadcastLog(final String log) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("who", "[StopAsyncTask]");
        broadcast.putExtra("msg", log);
        broadcast.setAction("tucson.node.LOG");
        this.nodeService.sendBroadcast(broadcast);
    }

    @Override
    protected String doInBackground(final String... params) {
        this.node.shutdown();
        return null;
    }

    @Override
    protected void onPostExecute(final String result) {
        this.nodeService.cancelNotifications();
        NodeStopAsyncTask.log("Node stopped");
        this.sendBroadcastLog("Node stopped");
        Toast.makeText(this.nodeService, R.string.serv_NodeService_stopped,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPreExecute() {
        NodeStopAsyncTask.log("Stopping node");
        this.sendBroadcastLog("Stopping node");
    }

    @Override
    protected void onProgressUpdate(final Void... values) {
        /*
         * Not used atm
         */
    }
}
