/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.node.service;

import it.unibo.tucson.android.MainActivity;
import it.unibo.tucson.android.R;
import it.unibo.tucson.android.node.TucsonNode;
import it.unibo.tucson.android.settings.SettingsFragment;
import alice.tucson.service.TucsonNodeService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class NodeService extends Service {

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */
    public class NodeServiceBinder extends Binder {

        /**
         *
         * @return the Node service
         */
        public NodeService getService() {
            return NodeService.this;
        }
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Tucson Service] " + s);
    }

    // This is the object that receives interactions from clients.
    private final IBinder mBinder = new NodeServiceBinder();
    private Context mContext;
    private TucsonNodeService node;
    private int port;
    private boolean running;

    /**
     * to unsubscribe from all the notifications
     */
    public void cancelNotifications() {
        final NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    /**
     *
     * @return wether the service is running
     */
    public boolean isRunning() {
        return this.running;
    }

    // Return binder se bindiamo, null se no
    @Override
    public IBinder onBind(final Intent intent) {
        return this.mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NodeService.log("Service created");
        this.sendBroadcastLog("Service created");
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getApplicationContext());
        this.port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsNodePort(), "20504"));
        this.node = new TucsonNodeService(this.port);
        this.mContext = TucsonNode.getContext();
        this.running = false;
    }

    @Override
    public void onDestroy() {
        this.cancelNotifications();
        NodeService.log("Killing node");
        this.sendBroadcastLog("Killing node");
        new NodeStopAsyncTask(this, this.node).execute();
        this.running = false;
        super.onDestroy();
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags,
            final int startId) {
        Log.i("NodeService", "Received start id " + startId + ": " + intent);
        NodeService.log("Starting node on port " + this.port);
        this.sendBroadcastLog("Starting node on port " + this.port);
        new NodeInstallAsyncTask(this, this.node, this.mContext).execute();
        this.running = true;
        /*
         * We want this service to continue running until it is explicitly
         * stopped, so return sticky.
         */
        return Service.START_STICKY;
    }

    /**
     * A service-related notification has been clicked, thus the service GUI has
     * to be shown
     */
    public void showNotification() {
        final CharSequence text = this
                .getText(R.string.serv_NodeService_started);
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this.mContext).setSmallIcon(R.drawable.tucson_icon)
                .setContentTitle("TuCSoN Node").setContentText(text);
        final Intent resultIntent = new Intent(this.mContext,
                MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        /*
         * Because clicking the notification opens a new ("special") activity,
         * there's no need to create an artificial back stack.
         */
        final PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this.mContext, 0, resultIntent, 0);
        mBuilder.setContentIntent(resultPendingIntent);
        final Notification notification = mBuilder.build();
        final NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    private void sendBroadcastLog(final String log) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("who", "[Tucson Service]");
        broadcast.putExtra("msg", log);
        broadcast.setAction("tucson.node.LOG");
        this.sendBroadcast(broadcast);
    }
}
