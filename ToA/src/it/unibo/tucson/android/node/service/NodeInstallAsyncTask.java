/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.node.service;

import it.unibo.tucson.android.R;
import alice.tucson.service.TucsonNodeService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class NodeInstallAsyncTask extends AsyncTask<String, Void, String> {

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[InstallAsyncTask] " + s);
    }

    private final Context mContext;
    private final TucsonNodeService node;
    private final NodeService nodeService;
    private ProgressDialog pdia;

    /**
     *
     * @param n
     *            the node service instance to install
     * @param ns
     *            the correspondant TuCSoN Node service
     * @param c
     *            the application context
     */
    public NodeInstallAsyncTask(final NodeService ns,
            final TucsonNodeService n, final Context c) {
        super();
        this.node = n;
        this.nodeService = ns;
        this.mContext = c;
    }

    private void sendBroadcastLog(final String log) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("who", "[InstallAsyncTask]");
        broadcast.putExtra("msg", log);
        broadcast.setAction("tucson.node.LOG");
        this.nodeService.sendBroadcast(broadcast);
    }

    @Override
    protected String doInBackground(final String... params) {
        this.node.install();
        return null;
    }

    @Override
    protected void onPostExecute(final String result) {
        NodeInstallAsyncTask.log("Node started");
        this.nodeService.showNotification();
        this.sendBroadcastLog("Node started");
        this.pdia.dismiss();
        this.pdia = null;
        Toast.makeText(this.nodeService, R.string.serv_NodeService_started,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPreExecute() {
        NodeInstallAsyncTask.log("Starting node");
        this.sendBroadcastLog("Starting node");
        this.pdia = new ProgressDialog(this.mContext);
        this.pdia.setCanceledOnTouchOutside(false);
        this.pdia.setCancelable(false);
        this.pdia.setMessage("Installing TuCSoN Node...");
        this.pdia.show();
    }

    @Override
    protected void onProgressUpdate(final Void... values) {
        /*
         * Not used atm
         */
    }
}
