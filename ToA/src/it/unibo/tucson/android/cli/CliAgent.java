/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.cli;

import java.util.List;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.logictuple.exceptions.InvalidTupleOperationException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.EnhancedACC;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tucson.parsing.MyOpManager;
import alice.tucson.parsing.TucsonOpParser;
import alice.tucson.service.TucsonCmd;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuprolog.Parser;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class CliAgent extends AbstractTucsonAgent {

    /**
     *
     * @param s
     */
    private static void log(final String s) {
        System.out.println("[CLI] " + s);
    }

    private Handler messageHandler;
    private String operation;
    private int port;
    private String tuplecenter, node;

    /**
     * @param handler
     *            the message handler for this app
     * @param aid
     *            the identifier of this TuCSoN agent
     * @param op
     *            the String representation of the operation to be performed
     * @param tc
     *            the identifier of the tuple centre target of the operation
     * @param n
     *            the String representation of the IP address of the of the
     *            TuCSoN node where the target tuple centre runs
     * @param p
     *            the IP port the target TuCSoN node is listening to
     * @throws TucsonInvalidAgentIdException
     *             if the given String is not a valid Prolog term
     */
    public CliAgent(final Handler handler, final String aid, final String op,
            final String tc, final String n, final int p)
                    throws TucsonInvalidAgentIdException {
        super(aid);
        this.tuplecenter = tc;
        this.operation = op;
        this.node = n;
        this.port = p;
        this.messageHandler = handler;
    }

    /**
     *
     * @param aid
     *            the identifier of this TuCSoN agent
     * @throws TucsonInvalidAgentIdException
     *             if the given String is not a valid Prolog term
     */
    public CliAgent(final String aid) throws TucsonInvalidAgentIdException {
        super(aid);
    }

    /**
     *
     */
    public void doOperation() {
        final EnhancedACC context = this.getContext();
        TucsonTupleCentreId tid = null;
        try {
            tid = new TucsonTupleCentreId(this.tuplecenter, this.node,
                    Integer.toString(this.port));
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
        }
        final TucsonOpParser parser = new TucsonOpParser(this.operation,
                this.node, this.port);
        try {
            parser.parse();
        } catch (final TucsonInvalidTupleCentreIdException e) {
            System.err.println("[CLI]: " + e);
            this.postError(e.toString());
        }
        final TucsonCmd cmd = parser.getCmd();
        tid = parser.getTid();
        if (cmd != null && tid != null) {
            this.postLog("requested operation -> ( cmd = " + cmd.getPrimitive()
                    + "(" + cmd.getArg() + ") )");
            this.postLog("target tuple centre -> ( tid = " + tid.getName()
                    + "@" + tid.getNode() + ":" + tid.getPort() + " )");
        }
        if (cmd == null || tid == null) {
            this.postUnknownCommand(this.operation);
        } else if ("quit".equals(cmd.getPrimitive())
                || "exit".equals(cmd.getPrimitive())) {
            try {
                this.postLog("Releasing ACC held (if any)...");
                context.exit();
            } catch (final TucsonOperationNotPossibleException ex) {
                System.err.println("[CLI]: " + ex);
                this.postError(ex.toString());
            } finally {
                this.postLog("Done! See you!");
            }
        } else {
            /**
             * Tokenize TuCSoN primitive & involved argument
             */
            final String methodName = cmd.getPrimitive();
            final String tuple = cmd.getArg();
            try {
                /**
                 * Admissible Ordinary primitives
                 */
                /*
                 * what about timeout? it returns null too...how to discriminate
                 * inp/rdp failure?
                 */
                if ("spawn".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.spawn(tid, t,
                            Long.MAX_VALUE);
                    if (op.isResultSuccess()) {
                        this.postLog("Operation succeeded.");
                        this.postResult("success");
                    } else {
                        this.postLog("Operation failed.");
                        this.postResult("failure");
                    }
                } else if ("out".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.out(tid, t,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("in".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.in(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("rd".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.rd(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("inp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.inp(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("rdp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.rdp(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("no".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.no(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("nop".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.nop(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("set".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.set(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleListResult(op);
                } else if ("get".equals(methodName)) {
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context
                            .get(tid, Long.MAX_VALUE);
                    this.elabTupleListResult(op);
                } else if ("out_all".equals(methodName)) {
                    final LogicTuple t = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.outAll(tid, t,
                            Long.MAX_VALUE);
                    this.elabTupleListResult(op);
                } else if ("rd_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.rdAll(tid, templ,
                            (Long) null);
                    this.elabTupleListResult(op);
                } else if ("no_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.noAll(tid, templ,
                            (Long) null);
                    this.elabTupleListResult(op);
                } else if ("in_all".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.inAll(tid, templ,
                            (Long) null);
                    this.elabTupleListResult(op);
                } else if ("urd".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.urd(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("uno".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.uno(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("urdp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.urdp(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("unop".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.unop(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("uin".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.uin(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("uinp".equals(methodName)) {
                    final LogicTuple templ = LogicTuple.parse(tuple);
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.uinp(tid, templ,
                            (Long) null);
                    this.elabTupleResult(op);
                } else if ("out_s".equals(methodName)) {
                    final LogicTuple t = new LogicTuple(Parser.parseSingleTerm(
                            "reaction(" + tuple + ")", new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.outS(tid,
                            new LogicTuple(t.getArg(0)),
                            new LogicTuple(t.getArg(1)),
                            new LogicTuple(t.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("in_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.inS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("rd_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.rdS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("inp_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.inpS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("rdp_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.rdpS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("no_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.noS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("nop_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm("reaction(" + tuple + ")",
                                    new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.nopS(tid,
                            new LogicTuple(templ.getArg(0)), new LogicTuple(
                                    templ.getArg(1)),
                                    new LogicTuple(templ.getArg(2)), Long.MAX_VALUE);
                    this.elabTupleResult(op);
                } else if ("set_s".equals(methodName)) {
                    final LogicTuple templ = new LogicTuple(
                            Parser.parseSingleTerm(tuple, new MyOpManager()));
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.setS(tid, templ,
                            Long.MAX_VALUE);
                    this.elabTupleListResult(op);
                } else if ("get_s".equals(methodName)) {
                    this.postElaborating(methodName, tuple, tid);
                    final ITucsonOperation op = context.getS(tid,
                            Long.MAX_VALUE);
                    this.elabTupleListResult(op);
                } else if ("help".equals(methodName)
                        || "man".equals(methodName)
                        || "syntax".equals(methodName)) {
                    this.postHelp();
                } else if ("o/".equals(methodName)) {
                    this.postLog("\\o");
                } else if ("\\o".equals(methodName)) {
                    this.postLog("o/");
                } else if ("hi".equalsIgnoreCase(methodName)) {
                    this.postLog("Hi there!");
                } else {
                    this.postUnknownCommand(methodName);
                }
            } catch (final InvalidLogicTupleException ex1) {
                System.err.println("[CLI]: " + ex1);
                this.postError(ex1.toString());
            } catch (final TucsonOperationNotPossibleException ex2) {
                System.err.println("[CLI]: " + ex2);
                this.postError(ex2.toString());
            } catch (final UnreachableNodeException ex3) {
                System.err.println("[CLI]: " + ex3);
                this.postError(ex3.toString());
            } catch (final OperationTimeOutException ex4) {
                System.err.println("[CLI]: " + ex4);
                this.postError(ex4.toString());
            } catch (final InvalidTupleOperationException ex5) {
                System.err.println("[CLI]: " + ex5);
                this.postError(ex5.toString());
            } catch (final NullPointerException ex6) {
                // Tucson node not available
                System.err.println("[CLI]: The TuCSoN Node is not available.");
                this.postError("The TuCSoN Node is not available.");
            }
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used here.
         */
    }

    private void elabTupleListResult(final ITucsonOperation op) {
        if (op.isResultSuccess()) {
            final List<Tuple> res = op.getTupleListResult();
            this.postLog("Operation succeeded. Result: " + res);
            this.postResult("success --> " + res);
        } else {
            final List<Tuple> res = op.getTupleListResult();
            this.postLog("Operation failed. Result: " + res);
            this.postResult("failure --> " + res);
        }
    }

    private void elabTupleResult(final ITucsonOperation op) {
        if (op.isResultSuccess()) {
            final Tuple res = op.getTupleResult();
            this.postLog("Operation succeeded. Result: " + res);
            this.postResult("success --> " + res);
        } else {
            final Tuple res = op.getTupleResult();
            this.postLog("Operation failed. Result: " + res);
            this.postResult("failure --> " + res);
        }
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[CLI]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postElaborating(final String methodName, final String tuple,
            final TucsonTupleCentreId tid) {
        CliAgent.log("requesting op " + methodName + ", " + tuple + ", " + tid);
        this.post("log", "requesting op " + methodName + ", " + tuple + ", "
                + tid);
        this.postResult("...");
    }

    private void postError(final String s) {
        CliAgent.log("Error: " + s);
        this.post("error", s);
    }

    private void postHelp() {
        CliAgent.log("--------------------------------------------------------------------------------");
        CliAgent.log("TuCSoN CLI Syntax:");
        CliAgent.log("");
        CliAgent.log("\t\ttcName@ipAddress:port ? CMD");
        CliAgent.log("");
        CliAgent.log("where CMD can be:");
        CliAgent.log("");
        CliAgent.log("\t\tout(Tuple)");
        CliAgent.log("\t\tin(TupleTemplate)");
        CliAgent.log("\t\trd(TupleTemplate)");
        CliAgent.log("\t\tno(TupleTemplate)");
        CliAgent.log("\t\tinp(TupleTemplate)");
        CliAgent.log("\t\trdp(TupleTemplate)");
        CliAgent.log("\t\tnop(TupleTemplate)");
        CliAgent.log("\t\tget()");
        CliAgent.log("\t\tset([Tuple1, ..., TupleN])");
        CliAgent.log("\t\tspawn(exec('Path.To.Java.Class.class')) | spawn(solve('Path/To/Prolog/Theory.pl', Goal))");
        CliAgent.log("\t\tin_all(TupleTemplate, TupleList)");
        CliAgent.log("\t\trd_all(TupleTemplate, TupleList)");
        CliAgent.log("\t\tno_all(TupleTemplate, TupleList)");
        CliAgent.log("\t\tuin(TupleTemplate)");
        CliAgent.log("\t\turd(TupleTemplate)");
        CliAgent.log("\t\tuno(TupleTemplate)");
        CliAgent.log("\t\tuinp(TupleTemplate)");
        CliAgent.log("\t\turdp(TupleTemplate)");
        CliAgent.log("\t\tunop(TupleTemplate)");
        CliAgent.log("\t\tout_s(Event,Guard,Reaction)");
        CliAgent.log("\t\tin_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        CliAgent.log("\t\trd_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        CliAgent.log("\t\tinp_s(EventTemplate, GuardTemplate ,ReactionTemplate)");
        CliAgent.log("\t\trdp_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        CliAgent.log("\t\tno_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        CliAgent.log("\t\tnop_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        CliAgent.log("\t\tget_s()");
        CliAgent.log("\t\tset_s([(Event1,Guard1,Reaction1), ..., (EventN,GuardN,ReactionN)])");
        CliAgent.log("--------------------------------------------------------------------------------");
        this.postLog("TuCSoN CLI Syntax:");
        this.postLog("");
        this.postLog("\t\ttcName@ipAddress:port ? CMD");
        this.postLog("");
        this.postLog("where CMD can be:");
        this.postLog("");
        this.postLog("\t\tout(Tuple)");
        this.postLog("\t\tin(TupleTemplate)");
        this.postLog("\t\trd(TupleTemplate)");
        this.postLog("\t\tno(TupleTemplate)");
        this.postLog("\t\tinp(TupleTemplate)");
        this.postLog("\t\trdp(TupleTemplate)");
        this.postLog("\t\tnop(TupleTemplate)");
        this.postLog("\t\tget()");
        this.postLog("\t\tset([Tuple1, ..., TupleN])");
        this.postLog("\t\tspawn(exec('Path.To.Java.Class.class')) | spawn(solve('Path/To/Prolog/Theory.pl', Goal))");
        this.postLog("\t\tin_all(TupleTemplate, TupleList)");
        this.postLog("\t\trd_all(TupleTemplate, TupleList)");
        this.postLog("\t\tno_all(TupleTemplate, TupleList)");
        this.postLog("\t\tuin(TupleTemplate)");
        this.postLog("\t\turd(TupleTemplate)");
        this.postLog("\t\tuno(TupleTemplate)");
        this.postLog("\t\tuinp(TupleTemplate)");
        this.postLog("\t\turdp(TupleTemplate)");
        this.postLog("\t\tunop(TupleTemplate)");
        this.postLog("\t\tout_s(Event,Guard,Reaction)");
        this.postLog("\t\tin_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        this.postLog("\t\trd_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        this.postLog("\t\tinp_s(EventTemplate, GuardTemplate ,ReactionTemplate)");
        this.postLog("\t\trdp_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        this.postLog("\t\tno_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        this.postLog("\t\tnop_s(EventTemplate, GuardTemplate, ReactionTemplate)");
        this.postLog("\t\tget_s()");
        this.postLog("\t\tset_s([(Event1,Guard1,Reaction1), ..., (EventN,GuardN,ReactionN)])");
    }

    private void postLog(final String s) {
        CliAgent.log(s);
        this.post("log", s);
    }

    private void postResult(final String s) {
        this.post("result", s);
    }

    private void postUnknownCommand(final String op) {
        this.post("error", "Unknown command <" + op + ">");
    }

    @Override
    protected void main() {
        this.doOperation();
    }
    // private void prompt(final String s) {
    // System.out.println(" -> " + s);
    // post("log", " -> " + s);
    // }
}
