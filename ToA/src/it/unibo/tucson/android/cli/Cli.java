/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.cli;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.settings.SettingsFragment;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class Cli extends Fragment {

    /*****************************************************************************************
     * MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                Cli.this.logTV.append(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
                Cli.this.appendLog(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                Cli.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                Cli.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
            } else if (bundle.containsKey("result")) {
                Cli.this.resultTV.setText(bundle.getString("result"));
            }
            Cli.this.performScrollDownOnLog();
        }
    }

    /**
     *
     * @param s
     */
    private static void log(final String s) {
        System.out.println("[CLI]:" + s);
    }

    private String log = "";
    private TextView logTV;
    private MessageHandler messageHandler;
    private String node;
    private int port;
    private TextView resultTV;
    private View rootView;
    private String tuplecenter;

    /**
     *
     * @param l
     *            the message to append to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.fragment_cli, container,
                false);
        this.rootView.findViewById(R.id.frag_cli_executeQuery)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                Cli.this.execute();
            }
        });
        this.rootView.findViewById(R.id.frag_cli_clearQuery)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                ((TextView) Cli.this.rootView
                        .findViewById(R.id.frag_cli_query)).setText("");
            }
        });
        if (this.messageHandler == null) {
            this.messageHandler = new MessageHandler();
        }
        this.logTV = (TextView) this.rootView.findViewById(R.id.frag_cli_log);
        this.resultTV = (TextView) this.rootView
                .findViewById(R.id.frag_cli_result);
        return this.rootView;
    }

    @Override
    public void onResume() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        this.tuplecenter = sharedPref.getString(
                SettingsFragment.getSettingsCliTupleCentre(), "default");
        this.node = sharedPref.getString(
                SettingsFragment.getSettingsCliIpAddress(), "localhost");
        this.port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsCliPort(), "20504"));
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_cli_query_header);
        view.setText("Querying on " + this.tuplecenter + "@" + this.node + ":"
                + this.port);
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        super.onResume();
    }

    private void execute() {
        final TextView queryTV = (TextView) this.rootView
                .findViewById(R.id.frag_cli_query);
        final String operation = queryTV.getText().toString();
        this.postLog("executing: " + operation);
        try {
            new CliAgent(this.messageHandler, "'cli-"
                    + System.currentTimeMillis() + "'", operation,
                    this.tuplecenter, this.node, this.port).go();
        } catch (final TucsonInvalidAgentIdException e) {
            e.printStackTrace();
        }
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_cli_log_sv).post(new Runnable() {

            @Override
            public void run() {
                ((ScrollView) Cli.this.rootView
                        .findViewById(R.id.frag_cli_log_sv))
                        .fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void postLog(final String s) {
        Cli.log(s);
        this.logTV.append("[CLI] " + s + "\n");
        this.appendLog("[CLI] " + s + "\n");
        this.performScrollDownOnLog();
    }
}
