/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.geolocation;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.settings.SettingsFragment;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class GeolocationFragment extends Fragment {

    private class PositionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("tucson.geolocation.service.LOG".equalsIgnoreCase(action)) {
                GeolocationFragment.this.logTV.append(intent
                        .getStringExtra("who")
                        + " "
                        + intent.getStringExtra("msg") + "\n");
                GeolocationFragment.this.appendLog(intent.getStringExtra("who")
                        + " " + intent.getStringExtra("msg") + "\n");
                GeolocationFragment.this.performScrollDownOnLog();
            } else if ("tucson.geolocation.service.ERROR"
                    .equalsIgnoreCase(action)) {
                GeolocationFragment.this.logTV.append(intent
                        .getStringExtra("who")
                        + " Error: "
                        + intent.getStringExtra("msg") + "\n");
                GeolocationFragment.this.appendLog(intent.getStringExtra("who")
                        + " Error: " + intent.getStringExtra("msg") + "\n");
                GeolocationFragment.this.performScrollDownOnLog();
            } else if ("tucson.geolocation.service.POSITION"
                    .equalsIgnoreCase(action)) {
                final String posType = intent.getStringExtra("posType");
                if ("ph".equals(posType)) {
                    GeolocationFragment.this.phText.setText(intent
                            .getStringExtra("loc"));
                } else if ("map".equals(posType)) {
                    GeolocationFragment.this.mapText.setText(intent
                            .getStringExtra("loc"));
                } else if ("org".equals(posType)) {
                    GeolocationFragment.this.orgText.setText(intent
                            .getStringExtra("loc"));
                } else if ("ip".equals(posType)) {
                    GeolocationFragment.this.ipText.setText(intent
                            .getStringExtra("loc"));
                } else if ("dns".equals(posType)) {
                    GeolocationFragment.this.dnsText.setText(intent
                            .getStringExtra("loc"));
                }
            }
        }
    }

    private static Context mContext;

    /**
     *
     * @return the application context in which this fragment is running
     */
    public static Context getContext() {
        return GeolocationFragment.mContext;
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void error(final String s) {
        System.out.println("[GeolocationManager] Error: " + s);
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[GeolocationManager] " + s);
    }

    private PositionReceiver broadcastReceiver;
    private String log = "";
    private TextView logTV;
    private TextView phText, mapText, orgText, ipText, dnsText;
    private View rootView;

    /**
     *
     * @param l
     *            the message to append to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    /**
     * To kill the Geolocation Service
     */
    public void destroyGeolocationService() {
        this.postLog("Destroying geolocation service..");
        Toast.makeText(GeolocationFragment.mContext,
                "Destroying geolocation service..", Toast.LENGTH_SHORT).show();
        try {
            new DestroyGeolocationServiceAgent(
                    "destroyGeolocationServiceAgent",
                    GeolocationFragment.mContext).go();
        } catch (final TucsonInvalidAgentIdException e) {
            this.postError("TucsonInvalidAgentIdException");
        }
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        sharedPref
        .edit()
        .putBoolean(SettingsFragment.getSettingsGeolocationExist(),
                false).commit();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout
        this.rootView = inflater.inflate(R.layout.fragment_geo, container,
                false);
        GeolocationFragment.mContext = this.getActivity();
        if (this.broadcastReceiver == null) {
            this.broadcastReceiver = new PositionReceiver();
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.geolocation.service.LOG");
        intentFilter.addAction("tucson.geolocation.service.ERROR");
        intentFilter.addAction("tucson.geolocation.service.POSITION");
        GeolocationFragment.mContext.registerReceiver(this.broadcastReceiver,
                intentFilter);
        this.phText = (TextView) this.rootView
                .findViewById(R.id.frag_geo_ph_text);
        this.mapText = (TextView) this.rootView
                .findViewById(R.id.frag_geo_map_text);
        this.orgText = (TextView) this.rootView
                .findViewById(R.id.frag_geo_org_text);
        this.ipText = (TextView) this.rootView
                .findViewById(R.id.frag_geo_ip_text);
        this.dnsText = (TextView) this.rootView
                .findViewById(R.id.frag_geo_dns_text);
        this.logTV = (TextView) this.rootView.findViewById(R.id.frag_geo_log);
        // StartService Button
        this.rootView.findViewById(R.id.buttonCreateService)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                GeolocationFragment.this.createGeolocationService();
            }
        });
        // StopService Button
        this.rootView.findViewById(R.id.buttonDestroyService)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                GeolocationFragment.this.destroyGeolocationService();
            }
        });
        return this.rootView;
    }

    @Override
    public void onResume() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        final int port = Integer.parseInt(sharedPref.getString(
                SettingsFragment.getSettingsGeolocationPort(), "20504"));
        final String tuplecentre = sharedPref
                .getString(
                        SettingsFragment.getSettingsGeolocationTupleCentre(),
                        "default");
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_geo_header);
        view.setText("Service on " + tuplecentre + "@localhost:" + port);
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        super.onResume();
    }

    @Override
    public void onStart() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tucson.geolocation.service.LOG");
        intentFilter.addAction("tucson.geolocation.service.ERROR");
        intentFilter.addAction("tucson.geolocation.service.POSITION");
        GeolocationFragment.mContext.registerReceiver(this.broadcastReceiver,
                intentFilter);
        super.onStart();
    }

    @Override
    public void onStop() {
        GeolocationFragment.mContext.unregisterReceiver(this.broadcastReceiver);
        super.onStop();
    }

    private void createGeolocationService() {
        this.postLog("Creating geolocation service..");
        Toast.makeText(GeolocationFragment.mContext,
                "Creating geolocation service..", Toast.LENGTH_SHORT).show();
        try {
            new CreateGeolocationServiceAgent("startGeolocationServiceAgent",
                    GeolocationFragment.mContext).go();
        } catch (final TucsonInvalidAgentIdException e) {
            this.postError("TucsonInvalidAgentIdException");
        }
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        sharedPref
        .edit()
        .putBoolean(SettingsFragment.getSettingsGeolocationExist(),
                true).commit();
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_geo_log_sv).post(new Runnable() {

            @Override
            public void run() {
                ((ScrollView) GeolocationFragment.this.rootView
                        .findViewById(R.id.frag_geo_log_sv))
                        .fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void postError(final String s) {
        GeolocationFragment.error(s);
        this.logTV.append("[GeolocationManager] Error: " + s + "\n");
        this.appendLog("[GeolocationManager] Error: " + s + "\n");
        this.performScrollDownOnLog();
    }

    private void postLog(final String s) {
        GeolocationFragment.log(s);
        this.logTV.append("[GeolocationManager] " + s + "\n");
        this.appendLog("[GeolocationManager] " + s + "\n");
        this.performScrollDownOnLog();
    }
}
