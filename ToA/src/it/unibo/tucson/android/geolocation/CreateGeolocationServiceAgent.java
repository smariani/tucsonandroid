/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.geolocation;

import it.unibo.tucson.android.settings.SettingsFragment;
import it.unibo.tucson.android.utils.Utils;
import java.io.IOException;
import java.io.InputStream;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class CreateGeolocationServiceAgent extends AbstractTucsonAgent {

    /**
     * ACC
     */
    private SynchACC acc;
    /**
     * Services tuple centres
     */
    private TucsonTupleCentreId geoService;
    /**
     * Activity context
     */
    private final Context mContext;
    /**
     * The id of the service to create
     */
    private String serviceId;
    /**
     * Geolocation configuration tuple centre
     */
    private TucsonTupleCentreId tcId;

    /**
     *
     * @param id
     *            the ID of this TuCSoN agent
     * @param context
     *            the application context within which it's executing
     * @throws TucsonInvalidAgentIdException
     *             if the given ID is not a valid TuCSoN agent ID
     */
    public CreateGeolocationServiceAgent(final String id, final Context context)
            throws TucsonInvalidAgentIdException {
        super(id);
        this.mContext = context;
        try {
            // Tuple centre used for geolocation service configuration
            this.tcId = new TucsonTupleCentreId("geolocationConfigTC",
                    "localhost", String.valueOf(20504));
            final SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(this.mContext);
            this.serviceId = sharedPref.getString(
                    SettingsFragment.getSettingsGeolocationId(),
                    "googleApiAndroidService");
            final int port = Integer.parseInt(sharedPref.getString(
                    SettingsFragment.getSettingsGeolocationPort(), "20504"));
            final String tuplecentre = sharedPref.getString(
                    SettingsFragment.getSettingsGeolocationTupleCentre(),
                    "default");
            // Tuple centre used for geolocation service interactions
            this.geoService = new TucsonTupleCentreId(tuplecentre, "localhost",
                    String.valueOf(port));
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.say("Error: " + "TucsonInvalidTupleCentreIdException");
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used atm
         */
    }

    private void createServices() throws TucsonOperationNotPossibleException,
    UnreachableNodeException, OperationTimeOutException {
        this.say("Creating service..");
        final LogicTuple t = new LogicTuple(
                "createGeolocationService",
                new Value(this.serviceId),
                new Value(
                        "it.unibo.tucson.android.geolocation.TucsonGeolocationService"),
                        new Value(this.geoService.toString()));
        this.acc.out(this.tcId, t, null);
        this.say("Service created");
    }

    private void injectSpecsInTCs() throws IOException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.say("Injecting 'geoService_spec' ReSpecT specification in tc < "
                + this.geoService.toString() + " >...");
        final InputStream is = this.mContext.getAssets().open(
                "geoService_spec.rsp");
        final LogicTuple specTuple = new LogicTuple("spec", new Value(
                Utils.fileToString(is)));
        this.acc.setS(this.geoService, specTuple, null);
    }

    @Override
    protected void main() {
        try {
            this.acc = this.getContext();
            this.say("Configuring application environment");
            this.createServices();
            this.injectSpecsInTCs();
            this.say("Configuration complete");
        } catch (final TucsonOperationNotPossibleException e) {
            this.say("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.say("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.say("OperationTimeOutException");
        } catch (final IOException e) {
            this.say(e.getMessage());
        }
    }
}
