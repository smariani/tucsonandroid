/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.geolocation;

import it.unibo.tucson.android.settings.SettingsFragment;
import alice.logictuple.LogicTuple;
import alice.logictuple.Value;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class DestroyGeolocationServiceAgent extends AbstractTucsonAgent {

    /**
     * ACC
     */
    private SynchACC acc;
    /**
     * The id of the service to create
     */
    private String serviceId;
    /**
     * Geolocation configuration tuple centre
     */
    private TucsonTupleCentreId tcId;

    /**
     *
     * @param id
     *            the ID of this TuCSoN agent
     * @param context
     *            the application context within which it's executing
     * @throws TucsonInvalidAgentIdException
     *             if the given ID is not a valid TuCSoN agent ID
     */
    public DestroyGeolocationServiceAgent(final String id, final Context context)
            throws TucsonInvalidAgentIdException {
        super(id);
        final Context mContext = context;
        try {
            // Tuple centre used for geolocation service configuration
            this.tcId = new TucsonTupleCentreId("geolocationConfigTC",
                    "localhost", String.valueOf(20504));
            final SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(mContext);
            this.serviceId = sharedPref.getString(
                    SettingsFragment.getSettingsGeolocationId(),
                    "googleApiAndroidService");
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.say("Error: " + "TucsonInvalidTupleCentreIdException");
        }
    }

    @Override
    public void operationCompleted(final ITucsonOperation op) {
        /*
         * Not used atm
         */
    }

    private void destroyServices() throws TucsonOperationNotPossibleException,
    UnreachableNodeException, OperationTimeOutException {
        this.say("Destroying service..");
        final LogicTuple t = new LogicTuple("destroyGeolocationService",
                new Value(this.serviceId));
        this.acc.out(this.tcId, t, null);
        this.say("Service destroyed");
    }

    @Override
    protected void main() {
        try {
            this.acc = this.getContext();
            this.destroyServices();
            this.say("Configuration complete");
        } catch (final TucsonOperationNotPossibleException e) {
            this.say("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.say("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.say("OperationTimeOutException");
        }
    }
}
