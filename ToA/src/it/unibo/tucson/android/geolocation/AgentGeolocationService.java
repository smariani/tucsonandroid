/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.geolocation;

import alice.respect.api.geolocation.service.AbstractGeolocationService;
import alice.respect.api.geolocation.service.GeoServiceId;
import alice.respect.api.place.DnsPlace;
import alice.respect.api.place.IPlace;
import alice.respect.api.place.IpPlace;
import alice.respect.api.place.MapPlace;
import alice.respect.api.place.OrgPlace;
import alice.respect.api.place.PhPlace;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tuprolog.Term;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class AgentGeolocationService extends AbstractGeolocationService {

    private class GeolocationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if ("agent.geolocation.service.LOCATION".equalsIgnoreCase(action)) {
                final String posType = intent.getStringExtra("posType");
                if (posType != null) {
                    if ("ph".equals(posType)) {
                        this.notifyLocationChanged(new PhPlace(intent
                                .getStringExtra("loc")));
                    } else if ("map".equals(posType)) {
                        this.notifyLocationChanged(new MapPlace(intent
                                .getStringExtra("loc")));
                    } else if ("org".equals(posType)) {
                        this.notifyLocationChanged(new OrgPlace(intent
                                .getStringExtra("loc")));
                    } else if ("ip".equals(posType)) {
                        this.notifyLocationChanged(new IpPlace(intent
                                .getStringExtra("loc")));
                    } else if ("dns".equals(posType)) {
                        this.notifyLocationChanged(new DnsPlace(intent
                                .getStringExtra("loc")));
                    }
                } else {
                    this.notifyLocationChanged(
                            intent.getDoubleExtra("lat", 0.0),
                            intent.getDoubleExtra("lng", 0.0));
                }
            } else if ("agent.geolocation.service.STARTMOVE"
                    .equalsIgnoreCase(action)) {
                if (AgentGeolocationService.this.genSpatialEvents) {
                    this.notifyStartMove(intent.getDoubleExtra("lat", 0.0),
                            intent.getDoubleExtra("lng", 0.0));
                }
            } else if ("agent.geolocation.service.STOPMOVE"
                    .equalsIgnoreCase(action)
                    && AgentGeolocationService.this.genSpatialEvents) {
                this.notifyStopMove(intent.getDoubleExtra("lat", 0.0),
                        intent.getDoubleExtra("lng", 0.0));
            }
        }

        private void notifyLocationChanged(final double lat, final double lng) {
            AgentGeolocationService.this.notifyLocationChanged(lat, lng);
        }

        private void notifyLocationChanged(final IPlace place) {
            AgentGeolocationService.this.notifyLocationChanged(place);
        }

        private void notifyStartMove(final double lat, final double lng) {
            AgentGeolocationService.this.notifyStartMovement(lat, lng);
        }

        private void notifyStopMove(final double lat, final double lng) {
            AgentGeolocationService.this.notifyStopMovement(lat, lng);
        }
    }

    private GeolocationReceiver broadcastReceiver;
    private final Context mContext;

    /**
     *
     * @param platform
     *            the type code of the platform this service is running on
     * @param sid
     *            the ID of the Geolocation Service
     * @param ttci
     *            the ID of the TuCSoN tuple centre responsible for this service
     */
    public AgentGeolocationService(final Integer platform,
            final GeoServiceId sid, final TucsonTupleCentreId ttci) {
        super(platform, sid, ttci);
        this.mContext = GeolocationFragment.getContext();
        if (this.broadcastReceiver == null) {
            this.broadcastReceiver = new GeolocationReceiver();
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("agent.geolocation.service.LOCATION");
        intentFilter.addAction("agent.geolocation.service.STARTMOVE");
        intentFilter.addAction("agent.geolocation.service.STOPMOVE");
        this.mContext.registerReceiver(this.broadcastReceiver, intentFilter);
    }

    @Override
    public Term geocode(final String address) {
        final String coords = AgentLocationService.geocode(address);
        return Term.createTerm(coords);
    }

    @Override
    public synchronized void start() {
        super.start();
        final Intent intent = new Intent(this.mContext,
                AgentLocationService.class);
        intent.putExtra("serviceId", this.getServiceId().getName());
        this.mContext.startService(intent);
    }

    @Override
    public synchronized void stop() {
        super.stop();
        this.mContext.unregisterReceiver(this.broadcastReceiver);
        final Intent intent = new Intent(this.mContext,
                AgentLocationService.class);
        this.mContext.stopService(intent);
    }
}
