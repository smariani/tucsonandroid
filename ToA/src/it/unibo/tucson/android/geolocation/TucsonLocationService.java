/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.geolocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class TucsonLocationService extends Service implements LocationListener {

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */
    public class LocationServiceBinder extends Binder {

        /**
         *
         * @return the TuCSoN Location Service
         */
        public TucsonLocationService getService() {
            return TucsonLocationService.this;
        }
    }

    private static Context appContext;
    /**
     * The distance to tread to generate from/to spatial events
     */
    private static final int EVENTS_GEN_GRANULARITY = 5;
    /**
     * Minimum distance interval between locations updates in meters
     */
    private static final int MIN_DIST_INTERVAL = 0;
    /**
     * Minimum time interval between locations updates in milliseconds
     */
    private static final int MIN_TIME_INTERVAL = 5000;

    /**
     *
     * @param toGeocode
     *            the String to convert into a String representation of a
     *            geocode
     * @return the String representation of a geocode
     */
    public static String geocode(final String toGeocode) {
        String lat = null;
        String lng = null;
        String coords = null;
        try {
            final Geocoder geocoder = new Geocoder(
                    TucsonLocationService.appContext);
            final List<Address> addresses = geocoder.getFromLocationName(
                    toGeocode, 5);
            final Address address = addresses.get(0);
            lat = String.valueOf(address.getLatitude());
            lng = String.valueOf(address.getLongitude());
            coords = "coords(" + lat + "," + lng + ")";
        } catch (final IOException e) {
            TucsonLocationService.error(
                    "[LocationService]",
                    "Unable to geocode the address " + toGeocode + ": "
                            + e.getMessage());
        }
        return coords;
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void error(final String id, final String s) {
        System.err.println("[" + id + "] Error: " + s);
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String id, final String s) {
        System.out.println("[" + id + "] " + s);
    }

    private LocationManager locationManager;
    private boolean moveStarted;
    private Location oldPos;
    private String serviceId;

    @Override
    public IBinder onBind(final Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        TucsonLocationService.appContext = this.getApplicationContext();
    }

    @Override
    public void onDestroy() {
        this.postLog("Stopping location service");
        this.locationManager.removeUpdates(this);
        this.postLog("Location service stopped");
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(final Location location) {
        // Getting latitude of the current location
        final double latitude = location.getLatitude();
        // Getting longitude of the current location
        final double longitude = location.getLongitude();
        // this.sendBroadcastLocation("ph", "coords(" + latitude + "," +
        // longitude + ")");
        this.sendBroadcastLocation(latitude, longitude);
        this.checkMovement(new Location(location));
        this.sendBroadcastLocation("map", this.getMapPlace(latitude, longitude));
        this.sendBroadcastLocation("org", this.getOrgPlace(latitude, longitude));
        this.sendBroadcastLocation("ip", this.getIpPlace());
        this.sendBroadcastLocation("dns", this.getDnsPlace());
    }

    @Override
    public void onProviderDisabled(final String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
        this.postLog("Disabled provider " + provider);
    }

    @Override
    public void onProviderEnabled(final String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
        this.postLog("Enabled new provider " + provider);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags,
            final int startId) {
        this.serviceId = intent.getStringExtra("serviceId");
        this.postLog("Received start id " + startId + ": " + intent);
        this.postLog("Starting location service");
        // Getting LocationManager object from System Service LOCATION_SERVICE
        this.locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        // Creating a criteria object to retrieve provider
        final Criteria criteria = new Criteria();
        // Getting the name of the best provider
        final String provider = this.locationManager.getBestProvider(criteria,
                true);
        // Getting Current Location
        final Location location = this.locationManager
                .getLastKnownLocation(provider);
        this.oldPos = new Location(location);
        if (location != null) {
            this.onLocationChanged(location);
        }
        this.locationManager.requestLocationUpdates(provider,
                TucsonLocationService.MIN_TIME_INTERVAL,
                TucsonLocationService.MIN_DIST_INTERVAL, this);
        this.postLog("Location service started");
        return Service.START_STICKY;
    }

    @Override
    public void onStatusChanged(final String provider, final int status,
            final Bundle extras) {
        /*
         * Not used atm
         */
    }

    private void checkMovement(final Location location) {
        final float distance = location.distanceTo(this.oldPos);
        if (!this.moveStarted
                && distance > TucsonLocationService.EVENTS_GEN_GRANULARITY) {
            /*
             * If the movement is not started yet and the device is moved for
             * more than 5 meters (in 5 seconds - MIN_TIME_INTERVAL) movement
             * started
             */
            this.sendBroadcastStartMove(this.oldPos);
            this.moveStarted = true;
        } else if (this.moveStarted
                && distance < TucsonLocationService.EVENTS_GEN_GRANULARITY) {
            /*
             * If the movement is started and the device is moved for less than
             * 5 meters (in 5 seconds - MIN_TIME_INTERVAL) movement terminated
             */
            this.sendBroadcastStopMove(location);
            this.moveStarted = false;
        }
        this.oldPos = new Location(location);
    }

    private String getDnsPlace() {
        String dnsPlace = null;
        try {
            final String extIP = new BufferedReader(
                    // Gets the external IP Address
                    new InputStreamReader(new URL(
                            "http://agentgatech.appspot.com").openStream()))
            .readLine();
            final InetAddress ad = InetAddress.getByName(extIP);
            dnsPlace = ad.getHostName();
        } catch (final UnknownHostException e) {
            this.postError("Unable to get dns place: " + e.getMessage());
        } catch (final MalformedURLException e) {
            this.postError("Unable to get dns place: " + e.getMessage());
        } catch (final IOException e) {
            this.postError("Unable to get dns place: " + e.getMessage());
        }
        if (dnsPlace == null) {
            return "NA";
        }
        return dnsPlace;
    }

    private String getIpPlace() {
        String ipPlace = null;
        try { // To get IP address when on 3G/WIFI connection
            for (final Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                final NetworkInterface intf = en.nextElement();
                for (final Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    final InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        final int networkPrefixL = NetworkInterface
                                .getByInetAddress(inetAddress)
                                /*
                                 * Gets the network prefix length of the
                                 * selected inet address
                                 */
                                .getInterfaceAddresses().get(1)
                                /*
                                 * get(0): returns IPv6 prefix; get(1): return
                                 * IPv4 prefix
                                 */
                                .getNetworkPrefixLength();
                        ipPlace = inetAddress.getHostAddress().toString() + "/"
                                + networkPrefixL;
                    }
                }
            }
        } catch (final SocketException e) {
            this.postError("Unable to get ip place: " + e.getMessage());
        }
        if (ipPlace == null) {
            return "NA";
        }
        return ipPlace;
    }

    private String getMapPlace(final double latitude, final double longitude) {
        String mapPlace = null;
        try {
            final Geocoder geocoder = new Geocoder(this.getApplicationContext());
            final List<Address> addresses = geocoder.getFromLocation(latitude,
                    longitude, 5);
            final Address address = addresses.get(0);
            mapPlace = String.format(
                    "%s, %s, %s",
                    // If there's a street address, add it
                    address.getMaxAddressLineIndex() > 0 ? address
                            .getAddressLine(0) : "",
                            // Locality is usually a city
                            address.getLocality(),
                            // The country of the address
                            address.getCountryName());
        } catch (final IOException e) {
            this.postError("Unable to get map place: " + e.getMessage());
        }
        if (mapPlace == null) {
            return "NA";
        }
        return mapPlace;
    }

    private String getOrgPlace(final double latitude, final double longitude) {
        String orgPlace = null;
        try {
            final Geocoder geocoder = new Geocoder(this.getApplicationContext());
            final List<Address> addresses = geocoder.getFromLocation(latitude,
                    longitude, 5);
            final Address address = addresses.get(0);
            orgPlace = String.format(
                    "%s, %s",
                    address.getMaxAddressLineIndex() > 0 ? address
                            .getAddressLine(0) : "", address.getLocality());
        } catch (final IOException e) {
            this.postError("Unable to get org place: " + e.getMessage());
        }
        if (orgPlace == null) {
            return "NA";
        }
        return "Workspace at " + orgPlace;
    }

    private void postError(final String s) {
        TucsonLocationService.error(this.serviceId, s);
        this.sendBroadcastError(s);
    }

    private void postLog(final String s) {
        TucsonLocationService.log(this.serviceId, s);
        this.sendBroadcastLog(s);
    }

    private void sendBroadcastError(final String error) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("who", "[" + this.serviceId + "]");
        broadcast.putExtra("msg", error);
        broadcast.setAction("tucson.geolocation.service.ERROR");
        this.sendBroadcast(broadcast);
    }

    private void sendBroadcastLocation(final double lat, final double lng) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("lat", lat);
        broadcast.putExtra("lng", lng);
        broadcast.setAction("tucson.geolocation.service.LOCATION");
        this.sendBroadcast(broadcast);
        this.sendBroadcastPosition("ph", "coords(" + lat + "," + lng + ")");
    }

    private void sendBroadcastLocation(final String type, final String loc) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("posType", type);
        broadcast.putExtra("loc", loc);
        broadcast.setAction("tucson.geolocation.service.LOCATION");
        this.sendBroadcast(broadcast);
        this.sendBroadcastPosition(type, loc);
    }

    private void sendBroadcastLog(final String log) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("who", "[" + this.serviceId + "]");
        broadcast.putExtra("msg", log);
        broadcast.setAction("tucson.geolocation.service.LOG");
        this.sendBroadcast(broadcast);
    }

    private void sendBroadcastPosition(final String type, final String loc) {
        /*
         * To update text views in GeolocationFragment
         */
        final Intent broadcast = new Intent();
        broadcast.putExtra("posType", type);
        broadcast.putExtra("loc", loc);
        broadcast.setAction("tucson.geolocation.service.POSITION");
        this.sendBroadcast(broadcast);
    }

    private void sendBroadcastStartMove(final Location l) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("lat", l.getLatitude());
        broadcast.putExtra("lng", l.getLongitude());
        broadcast.setAction("tucson.geolocation.service.STARTMOVE");
        this.sendBroadcast(broadcast);
    }

    private void sendBroadcastStopMove(final Location l) {
        final Intent broadcast = new Intent();
        broadcast.putExtra("lat", l.getLatitude());
        broadcast.putExtra("lng", l.getLongitude());
        broadcast.setAction("tucson.geolocation.service.STOPMOVE");
        this.sendBroadcast(broadcast);
    }
}
