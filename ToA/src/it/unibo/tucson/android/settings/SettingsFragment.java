/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.settings;

import it.unibo.tucson.android.R;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class SettingsFragment extends PreferenceFragment implements
OnSharedPreferenceChangeListener {

    private static final int DEF_PORT = 20504;
    private static final int MAX_PORT = 65535;
    private static final int MIN_PORT = 1024;
    private static final String SETTINGS_CLI_IPADDRESS = "pref_cli_ipAddress";
    private static final String SETTINGS_CLI_PORT = "pref_cli_port";
    private static final String SETTINGS_CLI_TUPLECENTRE = "pref_cli_tuplecentre";
    private static final String SETTINGS_CS_PEER_NAME = "pref_cs_peer_name";
    private static final String SETTINGS_CS_SERVER_IPADDRESS = "pref_cs_server_ipAddress";
    private static final String SETTINGS_CS_SERVER_PORT = "pref_cs_server_port";
    private static final String SETTINGS_CS_SERVER_TUPLECENTRE = "pref_cs_server_tuplecentre";
    private static final String SETTINGS_GEOLOCATION_EXIST = "pref_geolocation_exist";
    private static final String SETTINGS_GEOLOCATION_ID = "pref_geolocation_id";
    private static final String SETTINGS_GEOLOCATION_TUPLECENTRE = "pref_geolocation_tuplecentre";
    private static final String SETTINGS_INSPECTOR_IPADDRESS = "pref_inspector_ipAddress";
    private static final String SETTINGS_INSPECTOR_PORT = "pref_inspector_port";
    private static final String SETTINGS_INSPECTOR_TUPLECENTRE = "pref_inspector_tuplecentre";
    private static final String SETTINGS_NODE_PORT = "pref_node_port";
    private static final String SETTINGS_TESTS_IPADDRESS = "pref_tests_ipAddress";
    private static final String SETTINGS_TESTS_PORT = "pref_tests_port";
    private static final String SETTINGS_TESTS_TUPLECENTRE = "pref_tests_tuplecentre";

    /**
     * @return the settingsCliIpAddress
     */
    public static String getSettingsCliIpAddress() {
        return SettingsFragment.SETTINGS_CLI_IPADDRESS;
    }

    /**
     * @return the settingsCliPort
     */
    public static String getSettingsCliPort() {
        return SettingsFragment.SETTINGS_CLI_PORT;
    }

    /**
     * @return the settingsCliTupleCentre
     */
    public static String getSettingsCliTupleCentre() {
        return SettingsFragment.SETTINGS_CLI_TUPLECENTRE;
    }

    /**
     * @return the settingsCsPeerName
     */
    public static String getSettingsCsPeerName() {
        return SettingsFragment.SETTINGS_CS_PEER_NAME;
    }

    /**
     * @return the settingsCsServerIpAddress
     */
    public static String getSettingsCsServerIpAddress() {
        return SettingsFragment.SETTINGS_CS_SERVER_IPADDRESS;
    }

    /**
     * @return the settingsCsServerPort
     */
    public static String getSettingsCsServerPort() {
        return SettingsFragment.SETTINGS_CS_SERVER_PORT;
    }

    /**
     * @return the settingsCsServerTupleCentre
     */
    public static String getSettingsCsServerTupleCentre() {
        return SettingsFragment.SETTINGS_CS_SERVER_TUPLECENTRE;
    }

    /**
     *
     * @return the String representation of geolocation settings
     */
    public static String getSettingsGeolocationExist() {
        return SettingsFragment.SETTINGS_GEOLOCATION_EXIST;
    }

    /**
     * @return the settingsGeolocationId
     */
    public static String getSettingsGeolocationId() {
        return SettingsFragment.SETTINGS_GEOLOCATION_ID;
    }

    /**
     * @return the settingsGeolocationPort
     */
    public static String getSettingsGeolocationPort() {
        return SettingsFragment.SETTINGS_NODE_PORT;
    }

    /**
     * @return the settingsGeolocationTupleCentre
     */
    public static String getSettingsGeolocationTupleCentre() {
        return SettingsFragment.SETTINGS_GEOLOCATION_TUPLECENTRE;
    }

    /**
     * @return the settingsInspectorIpAddress
     */
    public static String getSettingsInspectorIpAddress() {
        return SettingsFragment.SETTINGS_INSPECTOR_IPADDRESS;
    }

    /**
     * @return the settingsInspectorPort
     */
    public static String getSettingsInspectorPort() {
        return SettingsFragment.SETTINGS_INSPECTOR_PORT;
    }

    /**
     * @return the settingsInspectorTupleCentre
     */
    public static String getSettingsInspectorTupleCentre() {
        return SettingsFragment.SETTINGS_INSPECTOR_TUPLECENTRE;
    }

    /**
     * @return the settingsNodePort
     */
    public static String getSettingsNodePort() {
        return SettingsFragment.SETTINGS_NODE_PORT;
    }

    /**
     * @return the settingsTestsIpAddress
     */
    public static String getSettingsTestsIpAddress() {
        return SettingsFragment.SETTINGS_TESTS_IPADDRESS;
    }

    /**
     * @return the settingsTestPort
     */
    public static String getSettingsTestsPort() {
        return SettingsFragment.SETTINGS_TESTS_PORT;
    }

    /**
     * @return the settingsTestTupleCentre
     */
    public static String getSettingsTestsTupleCentre() {
        return SettingsFragment.SETTINGS_TESTS_TUPLECENTRE;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        this.addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.getPreferenceScreen().getSharedPreferences()
        .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.getPreferenceScreen().getSharedPreferences()
        .registerOnSharedPreferenceChangeListener(this);
        // Sets the summaries
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(), SettingsFragment.SETTINGS_NODE_PORT);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_CLI_IPADDRESS);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_CLI_TUPLECENTRE);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(), SettingsFragment.SETTINGS_CLI_PORT);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_INSPECTOR_IPADDRESS);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_INSPECTOR_TUPLECENTRE);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_INSPECTOR_PORT);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_GEOLOCATION_ID);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_GEOLOCATION_TUPLECENTRE);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_TESTS_IPADDRESS);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_TESTS_TUPLECENTRE);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(), SettingsFragment.SETTINGS_TESTS_PORT);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_CS_SERVER_IPADDRESS);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_CS_SERVER_TUPLECENTRE);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(),
                SettingsFragment.SETTINGS_CS_SERVER_PORT);
        this.onSharedPreferenceChanged(this.getPreferenceScreen()
                .getSharedPreferences(), SettingsFragment.SETTINGS_CS_PEER_NAME);
    }

    /**
     * @param sharedPreferences
     *            the SharedPreferences
     * @param key
     *            the String representation of the preference key whose value
     *            has to be fetched
     */
    @Override
    public void onSharedPreferenceChanged(
            final SharedPreferences sharedPreferences, final String key) {
        String string = sharedPreferences.getString(key, "");
        if (key.equals(SettingsFragment.SETTINGS_NODE_PORT)) {
            final int port = Integer.parseInt(string);
            final Preference connectionPref = this.findPreference(key);
            if (port > SettingsFragment.MAX_PORT
                    | port < SettingsFragment.MIN_PORT) {
                string = Integer.toString(SettingsFragment.DEF_PORT);
                sharedPreferences.edit().putString(key, string).commit();
                Toast.makeText(this.getActivity().getApplicationContext(),
                        "Port selected not available, reset to default",
                        Toast.LENGTH_SHORT).show();
            }
            connectionPref.setSummary(this
                    .getString(R.string.pref_node_port_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CLI_IPADDRESS)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this.getString(R.string.pref_cli_ip_summ)
                    + "Current: " + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CLI_TUPLECENTRE)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_cli_tcname_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CLI_PORT)) {
            final int port = Integer.parseInt(string);
            final Preference connectionPref = this.findPreference(key);
            if (port > SettingsFragment.MAX_PORT
                    | port < SettingsFragment.MIN_PORT) {
                string = Integer.toString(SettingsFragment.DEF_PORT);
                sharedPreferences.edit().putString(key, string).commit();
                Toast.makeText(this.getActivity().getApplicationContext(),
                        "Port selected not available, reset to default",
                        Toast.LENGTH_SHORT).show();
            }
            connectionPref.setSummary(this
                    .getString(R.string.pref_cli_port_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_INSPECTOR_IPADDRESS)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_inspector_ip_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_INSPECTOR_TUPLECENTRE)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_inspector_tcname_summ)
                    + "Current: " + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_INSPECTOR_PORT)) {
            final int port = Integer.parseInt(string);
            final Preference connectionPref = this.findPreference(key);
            if (port > SettingsFragment.MAX_PORT
                    | port < SettingsFragment.MIN_PORT) {
                string = Integer.toString(SettingsFragment.DEF_PORT);
                sharedPreferences.edit().putString(key, string).commit();
                Toast.makeText(this.getActivity().getApplicationContext(),
                        "Port selected not available, reset to default",
                        Toast.LENGTH_SHORT).show();
            }
            connectionPref.setSummary(this
                    .getString(R.string.pref_inspector_port_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_GEOLOCATION_ID)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_geolocation_id_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_GEOLOCATION_TUPLECENTRE)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_geolocation_tcname_summ)
                    + "Current: " + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_TESTS_IPADDRESS)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_tests_ip_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_TESTS_TUPLECENTRE)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_tests_tcname_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_TESTS_PORT)) {
            final int port = Integer.parseInt(string);
            final Preference connectionPref = this.findPreference(key);
            if (port > SettingsFragment.MAX_PORT
                    | port < SettingsFragment.MIN_PORT) {
                string = Integer.toString(SettingsFragment.DEF_PORT);
                sharedPreferences.edit().putString(key, string).commit();
                Toast.makeText(this.getActivity().getApplicationContext(),
                        "Port selected not available, reset to default",
                        Toast.LENGTH_SHORT).show();
            }
            connectionPref.setSummary(this
                    .getString(R.string.pref_tests_port_summ)
                    + "Current:  "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CS_SERVER_IPADDRESS)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_cs_server_ip_summ)
                    + "Current: "
                    + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CS_SERVER_TUPLECENTRE)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_cs_server_tcname_summ)
                    + "Current: " + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CS_SERVER_PORT)) {
            final int port = Integer.parseInt(string);
            final Preference connectionPref = this.findPreference(key);
            if (port > SettingsFragment.MAX_PORT
                    | port < SettingsFragment.MIN_PORT) {
                string = Integer.toString(SettingsFragment.DEF_PORT);
                sharedPreferences.edit().putString(key, string).commit();
                Toast.makeText(this.getActivity().getApplicationContext(),
                        "Port selected not available, reset to default",
                        Toast.LENGTH_SHORT).show();
            }
            connectionPref.setSummary(this
                    .getString(R.string.pref_cs_server_port_summ)
                    + "Current:  " + string);
        }
        if (key.equals(SettingsFragment.SETTINGS_CS_PEER_NAME)) {
            final Preference connectionPref = this.findPreference(key);
            connectionPref.setSummary(this
                    .getString(R.string.pref_cs_peer_name_summ)
                    + "Current: "
                    + string);
        }
    }
}
