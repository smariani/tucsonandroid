/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.settings;

import it.unibo.tucson.android.MainActivity;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class SettingsActivity extends Activity {

    // When we select the home button we should go back to base
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            final Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            this.startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = this.getActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Display the fragment as the main content.
        this.getFragmentManager().beginTransaction()
        .replace(android.R.id.content, new SettingsFragment()).commit();
    }
}
