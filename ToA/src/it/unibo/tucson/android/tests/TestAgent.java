/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.tests;

import java.util.Calendar;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.respect.api.geolocation.Position;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tucson.service.ACCProxyAgentSide;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuprolog.Var;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class TestAgent extends AbstractTucsonAgent {

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Android Agent] " + s);
    }

    private SynchACC acc;
    private final String ip;
    private final Handler messageHandler;
    private final String port;
    private TucsonTupleCentreId tcid;
    private final int testToDo;
    private final String tuplecentre;

    /**
     *
     * @param aid
     *            the String representation of the identifier of this TuCSoN
     *            agent
     * @param addr
     *            the String representation of the IP address of the TuCSoN node
     *            hosting the target tuple centre
     * @param tc
     *            the String representation of the identifier of the target
     *            tuple centre
     * @param p
     *            the String representation of the listening port of the TuCSoN
     *            node hosting the target tuple centre
     * @param h
     *            the Handler
     * @param ttd
     *            the integer type code of the test to be done
     * @throws TucsonInvalidAgentIdException
     *             if the given String is not a valid representation of a Prolog
     *             term
     */
    protected TestAgent(final String aid, final String addr, final String tc,
            final String p, final Handler h, final int ttd)
                    throws TucsonInvalidAgentIdException {
        super(aid);
        this.ip = addr;
        this.testToDo = ttd;
        this.tuplecentre = tc;
        this.port = p;
        this.messageHandler = h;
    }

    @Override
    public void operationCompleted(final ITucsonOperation arg0) {
        /*
         * Not used here.
         */
    }

    private void addTestReaction(final LogicTuple event,
            final LogicTuple guards, final LogicTuple body) {
        try {
            this.acc.outS(this.tcid, event, guards, body, null);
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        }
    }

    private boolean doAtTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'guardAt_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is = this.mContext.getAssets().open("guardAt_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(testAt(S,P)),
        // (completion, operation, at(S,P)),
        // (
        // out(atRes(P))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("testAt(S,P)");
        final LogicTuple guards = LogicTuple
                .parse("( completion, operation, at(S,P) )");
        final LogicTuple body = LogicTuple.parse("( out(atRes(P)) )");
        this.addTestReaction(event, guards, body);
        // reaction(
        // out(atRes(P)),
        // (internal),
        // (
        // in(testAt(S,X))
        // )
        // ).
        final LogicTuple event2 = LogicTuple.parse("out(atRes(P))");
        final LogicTuple guards2 = LogicTuple.parse("( internal )");
        final LogicTuple body2 = LogicTuple.parse("( in(testAt(S,X)) )");
        this.addTestReaction(event2, guards2, body2);
        this.postLog("I'm testing guard 'at' ph...");
        final Position p = ((ACCProxyAgentSide) this.acc).getPosition();
        final LogicTuple template = LogicTuple.parse("testAt(ph,"
                + p.getPlace("ph") + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("guard 'at' failed");
            this.postLog("Removing 'guardAt_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return false;
        }
        op = this.acc.in(this.tcid, LogicTuple.parse("atRes(Pos)"), null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("guard 'at' completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("guard 'at' result is " + res);
            this.postLog("Removing 'guardAt_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return true;
        }
        this.postLog("guard 'at' failed");
        this.postLog("Removing 'guardAt_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        return false;
    }

    private boolean doCurrentPlaceTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'currentPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("currentPlace_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(testCurrentPlace(S,P)),
        // (completion, operation),
        // (
        // current_place(S,P),
        // out(currentPlace(P))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("out(testCurrentPlace(S,P))");
        final LogicTuple guards = LogicTuple.parse("( completion, operation )");
        final LogicTuple body = LogicTuple.parse("( " + "current_place(S,P),"
                + "out(currentPlace(P)) )");
        this.addTestReaction(event, guards, body);
        // reaction(
        // out(currentPlace(P)),
        // (internal),
        // (
        // in(testCurrentPlace(S,X))
        // )
        // ).
        final LogicTuple event2 = LogicTuple.parse("out(currentPlace(P))");
        final LogicTuple guards2 = LogicTuple.parse("( internal )");
        final LogicTuple body2 = LogicTuple
                .parse("( in(testCurrentPlace(S,X)) )");
        this.addTestReaction(event2, guards2, body2);
        this.postLog("I'm testing current_place ph...");
        final LogicTuple template = LogicTuple.parse("testCurrentPlace(ph,"
                + new Var("P") + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("current_place failed");
            this.postLog("Removing 'currentPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return false;
        }
        op = this.acc
                .in(this.tcid, LogicTuple.parse("currentPlace(Pos)"), null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("current_place completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("current_place result is " + res);
            this.postLog("Removing 'currentPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return true;
        }
        this.postLog("current_place failed");
        this.postLog("Removing 'currentPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        return false;
    }

    private boolean doEventPlaceTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'eventPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("eventPlace_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(testEventPlace(S,P)),
        // (completion, operation),
        // (
        // event_place(S,P),
        // out(eventPlace(P))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("out(testEventPlace(S,P))");
        final LogicTuple guards = LogicTuple.parse("( completion, operation )");
        final LogicTuple body = LogicTuple.parse("( " + "event_place(S,P),"
                + "out(eventPlace(P)) )");
        this.addTestReaction(event, guards, body);
        // reaction(
        // out(eventPlace(P)),
        // (internal),
        // (
        // in(testEventPlace(S,X))
        // )
        // ).
        final LogicTuple event2 = LogicTuple.parse("out(eventPlace(P))");
        final LogicTuple guards2 = LogicTuple.parse("( internal )");
        final LogicTuple body2 = LogicTuple
                .parse("( in(testEventPlace(S,X)) )");
        this.addTestReaction(event2, guards2, body2);
        this.postLog("I'm testing start_place ph...");
        final LogicTuple template = LogicTuple.parse("testEventPlace(ph,"
                + new Var("P") + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("event_place failed");
            this.postLog("Removing 'eventPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return false;
        }
        op = this.acc.in(this.tcid, LogicTuple.parse("eventPlace(Pos)"), null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("event_place completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("event_place result is " + res);
            this.postLog("Removing 'eventPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return true;
        }
        this.postLog("event_place failed");
        this.postLog("Removing 'eventPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        return false;
    }

    private boolean doNearTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'guardNear_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("guardNear_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(testNear(S,P,R)),
        // (completion, operation, near(S,P,R)),
        // (
        // out(nearRes(P))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("out(testNear(S,P,R))");
        final LogicTuple guards = LogicTuple
                .parse("( completion, operation, near(S,P,R) )");
        final LogicTuple body = LogicTuple.parse("( out(nearRes(P)) )");
        this.addTestReaction(event, guards, body);
        // reaction(
        // out(nearRes(P)),
        // (internal),
        // (
        // in(testNear(S,X,Y))
        // )
        // ).
        final LogicTuple event2 = LogicTuple.parse("out(nearRes(P))");
        final LogicTuple guards2 = LogicTuple.parse("( internal )");
        final LogicTuple body2 = LogicTuple.parse("( in(testNear(S,X,Y)) )");
        this.addTestReaction(event2, guards2, body2);
        this.postLog("I'm testing guard 'near' ph...");
        final Position p = ((ACCProxyAgentSide) this.acc).getPosition();
        final LogicTuple template = LogicTuple.parse("testNear(ph,"
                + p.getPlace("ph") + "," + 2 + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("guard 'near' failed");
            this.postLog("Removing 'guardNear_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return false;
        }
        op = this.acc.in(this.tcid, LogicTuple.parse("nearRes(Pos)"), null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("guard 'near' completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("guard 'near' result is " + res);
            this.postLog("Removing 'guardNear_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            return true;
        }
        this.postLog("guard 'near' failed");
        this.postLog("Removing 'guardNear_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        return false;
    }

    private boolean doReadTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        // Read
        final LogicTuple template = LogicTuple.parse("msg(Who)");
        final ITucsonOperation op = this.acc.in(this.tcid, template,
                (long) 10000);
        // Read Check
        if (op.isResultSuccess()) {
            this.postLog("op in," + template + ", " + this.tcid
                    + " completed successfully");
            return true;
        }
        this.postLog("op in," + template + ", " + this.tcid + " failed");
        return false;
    }

    private boolean doStartPlaceTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'startPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("startPlace_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(testStartPlace(S,P)),
        // (completion, operation),
        // (
        // out(startPlaceTrigger(S,P))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("out(testStartPlace(S,P))");
        final LogicTuple guards = LogicTuple.parse("( completion, operation )");
        final LogicTuple body = LogicTuple
                .parse("( out(startPlaceTrigger(S,P)) )");
        this.addTestReaction(event, guards, body);
        // %triggered reaction from out(testStartPlace(S,P)), creating a chain
        // of events
        // reaction(
        // out(startPlaceTrigger(S,P)),
        // (internal),
        // (
        // start_place(S,P),
        // out(startPlace(P))
        // )
        // ).
        final LogicTuple event2 = LogicTuple
                .parse("out(startPlaceTrigger(S,P))");
        final LogicTuple guards2 = LogicTuple.parse("( internal )");
        final LogicTuple body2 = LogicTuple.parse("( " + "start_place(S,P),"
                + "out(startPlace(P)) )");
        this.addTestReaction(event2, guards2, body2);
        // reaction(
        // out(startPlace(P)),
        // (internal),
        // (
        // in(testStartPlace(S,X)),
        // in(startPlaceTrigger(S,Y))
        // )
        // ).
        final LogicTuple event3 = LogicTuple.parse("out(startPlace(P))");
        final LogicTuple guards3 = LogicTuple.parse("( internal )");
        final LogicTuple body3 = LogicTuple.parse("( "
                + "in(testStartPlace(S,X))," + "in(startPlaceTrigger(S,Y)) )");
        this.addTestReaction(event3, guards3, body3);
        this.postLog("I'm testing start_place ph...");
        final LogicTuple template = LogicTuple.parse("testStartPlace(ph,"
                + new Var("P") + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("current_place failed");
            this.postLog("Removing 'startPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            this.removeTestReaction(event3, guards3, body3);
            return false;
        }
        op = this.acc.in(this.tcid, LogicTuple.parse("startPlace(Pos)"), null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("start_place completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("start_place result is " + res);
            this.postLog("Removing 'startPlace_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            this.removeTestReaction(event3, guards3, body3);
            return true;
        }
        this.postLog("start_place failed");
        this.postLog("Removing 'startPlace_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        this.removeTestReaction(event3, guards3, body3);
        return false;
    }

    private boolean doWriteReadTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        // Write
        final LogicTuple tuple = LogicTuple.parse("msg('Hi, I am Android!')");
        ITucsonOperation op = this.acc.out(this.tcid, tuple, (long) 10000);
        this.postLog("requesting op " + "out" + ", " + tuple + ", " + this.tcid);
        // Write Check
        if (op.isResultSuccess()) {
            this.postLog("op out," + tuple + ", " + this.tcid
                    + " completed successfully");
            // Read
            final LogicTuple template = LogicTuple.parse("msg(Who)");
            op = this.acc.in(this.tcid, template, (long) 10000);
            this.postLog("requesting op " + "in" + ", " + template + ", "
                    + this.tcid);
            // Read Check
            if (op.isResultSuccess()) {
                this.postLog("op in," + template + ", " + this.tcid
                        + " completed successfully");
                return true;
            }
            this.postLog("op in," + template + ", " + this.tcid + " failed");
            return false;
        }
        this.postLog("op out," + tuple + ", " + this.tcid + " failed");
        return false;
    }

    private boolean doWriteTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        // Write
        final LogicTuple tuple = LogicTuple.parse("msg('Hi, I am Android!')");
        final ITucsonOperation op = this.acc
                .out(this.tcid, tuple, (long) 10000);
        // Write Check
        if (op.isResultSuccess()) {
            this.postLog("op out," + tuple + ", " + this.tcid
                    + " completed successfully");
            return true;
        }
        this.postLog("op out," + tuple + ", " + this.tcid + " failed");
        return false;
    }

    private boolean motionLogTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException, InterruptedException {
        this.postLog("Injecting 'motionLog_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("motionLog_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // out(triggerFrom(StartP)),
        // (completion, operation),
        // (
        // from(ph,StartP)
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("out(triggerFrom(StartP))");
        final LogicTuple guards = LogicTuple.parse("( completion, operation )");
        final LogicTuple body = LogicTuple.parse("( from(ph,StartP) )");
        this.addTestReaction(event, guards, body);
        // reaction(
        // out(triggerTo(ArrP)),
        // (completion, operation),
        // (
        // to(ph,ArrP)
        // )
        // ).
        final LogicTuple event2 = LogicTuple.parse("out(triggerTo(ArrP))");
        final LogicTuple guards2 = LogicTuple
                .parse("( completion, operation )");
        final LogicTuple body2 = LogicTuple.parse("( to(ph,ArrP) )");
        this.addTestReaction(event2, guards2, body2);
        // reaction( from(ph,StartP), true,
        // ( current_time(StartT),
        // out(start_log(StartP,StartT)) )).
        final LogicTuple event3 = LogicTuple.parse("from(ph,StartP)");
        final LogicTuple guards3 = LogicTuple.parse("( true )");
        final LogicTuple body3 = LogicTuple.parse("( "
                + "current_time(StartT)," + "out(start_log(StartP,StartT)) )");
        this.addTestReaction(event3, guards3, body3);
        // reaction( to(ph,ArrP), true,
        // ( current_time(ArrT),
        // out(stop_log(ArrP,ArrT)) )).
        final LogicTuple event4 = LogicTuple.parse("to(ph,ArrP)");
        final LogicTuple guards4 = LogicTuple.parse("( true )");
        final LogicTuple body4 = LogicTuple.parse("( " + "current_time(ArrT),"
                + "out(stop_log(ArrP,ArrT)) )");
        this.addTestReaction(event4, guards4, body4);
        // reaction( out(stop_log(ArrP,ArrT)),
        // internal,
        // ( in(triggerTo(X)),
        // in(triggerFrom(Y))),
        // in(start_log(StartP,StartT)),
        // in(stop_log(ArrP,ArrT)),
        // out(m_log(StartP,ArrP,StartT,ArrT)) )).
        final LogicTuple event5 = LogicTuple.parse("out(stop_log(ArrP,ArrT))");
        final LogicTuple guards5 = LogicTuple.parse("( internal )");
        final LogicTuple body5 = LogicTuple.parse("( " + "in(triggerTo(X)),"
                + "in(triggerFrom(Y)))," + "in(start_log(StartP,StartT)),"
                + "in(stop_log(ArrP,ArrT)),"
                + "out(m_log(StartP,ArrP,StartT,ArrT)) )");
        this.addTestReaction(event5, guards5, body5);
        this.postLog("I'm testing motion log example...");
        Position p = ((ACCProxyAgentSide) this.acc).getPosition();
        LogicTuple template = LogicTuple.parse("triggerFrom("
                + p.getPlace("ph") + ")");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("test failed");
            this.postLog("Removing 'motionLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            this.removeTestReaction(event3, guards3, body3);
            this.removeTestReaction(event4, guards4, body4);
            this.removeTestReaction(event5, guards5, body5);
            return false;
        }
        this.wait(2000);
        p = ((ACCProxyAgentSide) this.acc).getPosition();
        template = LogicTuple.parse("triggerTo(" + p.getPlace("ph") + ")");
        op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("test failed");
            this.postLog("Removing 'motionLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            this.removeTestReaction(event3, guards3, body3);
            this.removeTestReaction(event4, guards4, body4);
            this.removeTestReaction(event5, guards5, body5);
            return false;
        }
        template = LogicTuple.parse("m_log(StartP,ArrP,StartT,ArrT)");
        op = this.acc.in(this.tcid, template, null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("test completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("test result is " + res);
            this.postLog("Removing 'motionLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            this.removeTestReaction(event2, guards2, body2);
            this.removeTestReaction(event3, guards3, body3);
            this.removeTestReaction(event4, guards4, body4);
            this.removeTestReaction(event5, guards5, body5);
            return true;
        }
        this.postLog("movement log test failed");
        this.postLog("Removing 'motionLog_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        this.removeTestReaction(event2, guards2, body2);
        this.removeTestReaction(event3, guards3, body3);
        this.removeTestReaction(event4, guards4, body4);
        this.removeTestReaction(event5, guards5, body5);
        return false;
    }

    private void post(final String what, final String s) {
        final Bundle bundle = new Bundle();
        bundle.putString("who", "[Android Agent]");
        bundle.putString(what, s);
        final Message message = this.messageHandler.obtainMessage();
        message.setData(bundle);
        this.messageHandler.sendMessage(message);
    }

    private void postError(final String s) {
        TestAgent.log("Error: " + s);
        this.post("error", s);
    }

    private void postLog(final String s) {
        TestAgent.log(s);
        this.post("log", s);
    }

    private void removeTestReaction(final LogicTuple event,
            final LogicTuple guards, final LogicTuple body) {
        try {
            this.acc.inS(this.tcid, event, guards, body, null);
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError("TucsonOperationNotPossibleException");
        } catch (final UnreachableNodeException e) {
            this.postError("UnreachableNodeException");
        } catch (final OperationTimeOutException e) {
            this.postError("OperationTimeOutException");
        }
    }

    private boolean spatialLogTest() throws InvalidLogicTupleException,
    TucsonOperationNotPossibleException, UnreachableNodeException,
    OperationTimeOutException {
        this.postLog("Injecting 'spatialLog_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        // InputStream is =
        // this.mContext.getAssets().open("spatialLog_spec.rsp");
        // LogicTuple specTuple = new LogicTuple("spec", new
        // Value(Utils.fileToString(is)) );
        // this.acc.setS(this.tcid,
        // specTuple,
        // null);
        // reaction(
        // in(q(X)),
        // (completion, operation),
        // (
        // current_place(ph,DevPos),
        // event_place(ph,AgentPos),
        // out(in_log(AgentPos,DevPos,q(X)))
        // )
        // ).
        final LogicTuple event = LogicTuple.parse("in(q(X))");
        final LogicTuple guards = LogicTuple.parse("( completion, operation )");
        final LogicTuple body = LogicTuple.parse("( "
                + "current_place(ph,DevPos)," + "event_place(ph,AgentPos),"
                + "out(in_log(AgentPos,DevPos,q(X))) )");
        this.addTestReaction(event, guards, body);
        this.postLog("I'm preparing the tuple centre for the test...");
        LogicTuple template = LogicTuple.parse("q(test)");
        ITucsonOperation op = this.acc.out(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("test failed");
            this.postLog("Removing 'spatialLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            return false;
        }
        this.postLog("I'm testing spatial log example...");
        template = LogicTuple.parse("q(X)");
        op = this.acc.in(this.tcid, template, null);
        if (!op.isResultSuccess()) {
            this.postLog("test failed");
            this.postLog("Removing 'spatialLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            return false;
        }
        template = LogicTuple.parse("in_log(AgentPos,DevPos,q(X))");
        op = this.acc.in(this.tcid, template, null);
        LogicTuple res = null;
        if (op.isResultSuccess()) {
            this.postLog("test completed successfully");
            res = op.getLogicTupleResult();
            this.postLog("test result is " + res);
            this.postLog("Removing 'spatialLog_spec' ReSpecT specification in tc < "
                    + this.tcid.toString() + " >...");
            this.removeTestReaction(event, guards, body);
            return true;
        }
        this.postLog("spatial log test failed");
        this.postLog("Removing 'spatialLog_spec' ReSpecT specification in tc < "
                + this.tcid.toString() + " >...");
        this.removeTestReaction(event, guards, body);
        return false;
    }

    @Override
    protected void main() {
        this.acc = this.getContext();
        // Attaching geolocation service to the agent
        TucsonTupleCentreId tcId;
        try {
            tcId = new TucsonTupleCentreId(this.myName()
                    + "_geolocation_service", "localhost",
                    String.valueOf(this.port));
            ((ACCProxyAgentSide) this.acc)
            .attachGeolocationService(
                    "it.unibo.tucson.android.geolocation.AgentGeolocationService",
                    tcId);
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError("creating geolocation service -> "
                    + "TucsonInvalidTupleCentreIdException");
        }
        final Calendar cal = Calendar.getInstance();
        try {
            this.tcid = new TucsonTupleCentreId(this.tuplecentre, this.ip,
                    this.port);
            boolean success = false;
            final long startTime = System.currentTimeMillis();
            this.postLog("test started at " + cal.get(Calendar.HOUR_OF_DAY)
                    + ":" + cal.get(Calendar.MINUTE) + ":"
                    + cal.get(Calendar.SECOND));
            switch (this.testToDo) {
                case Tests.WRITE:
                    success = this.doWriteTest();
                    break;
                case Tests.READ:
                    success = this.doReadTest();
                    break;
                case Tests.READWRITE:
                    success = this.doWriteReadTest();
                    break;
                case Tests.CURRENT_PLACE:
                    success = this.doCurrentPlaceTest();
                    break;
                case Tests.START_PLACE:
                    success = this.doStartPlaceTest();
                    break;
                case Tests.EVENT_PLACE:
                    success = this.doEventPlaceTest();
                    break;
                case Tests.AT:
                    success = this.doAtTest();
                    break;
                case Tests.NEAR:
                    success = this.doNearTest();
                    break;
                case Tests.SPATIAL_LOG:
                    success = this.spatialLogTest();
                    break;
                case Tests.MOTION_LOG:
                    success = this.motionLogTest();
                    break;
                default:
                    break;
            }
            final long ping = System.currentTimeMillis() - startTime;
            this.postLog("test finished at " + cal.get(Calendar.HOUR_OF_DAY)
                    + ":" + cal.get(Calendar.MINUTE) + ":"
                    + cal.get(Calendar.SECOND));
            if (success) {
                this.postLog("test completed in " + ping + "ms");
            } else {
                this.postLog("test failed");
            }
            this.postLog("------------------------------");
        } catch (final TucsonInvalidTupleCentreIdException e) {
            this.postError(e.toString());
        } catch (final InvalidLogicTupleException e) {
            this.postError(e.toString());
            e.printStackTrace();
        } catch (final TucsonOperationNotPossibleException e) {
            this.postError(e.toString());
        } catch (final UnreachableNodeException e) {
            this.postError(e.toString());
        } catch (final OperationTimeOutException e) {
            this.postError(e.toString());
        } catch (final InterruptedException e) {
            this.postError(e.toString());
        }
    }
}
