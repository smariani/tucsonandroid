/*
 * Copyright 1999-2014 Alma Mater Studiorum - Universita' di Bologna This file
 * is part of TuCSoNAndroid <http://tucsonandroid.apice.unibo.it>. TuCSoNAndroid
 * is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. TuCSoNAndroid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details. You should have received a copy of the GNU Lesser General
 * Public License along with TuCSoNAndroid. If not, see
 * <https://www.gnu.org/licenses/lgpl.html>.
 */
package it.unibo.tucson.android.tests;

import it.unibo.tucson.android.R;
import it.unibo.tucson.android.settings.SettingsFragment;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 * @author Michele Bombardi (mailto: michele.bombardi@studio.unibo.it)
 * @author (contributor) Stefano Mariani (mailto: s.mariani@unibo.it)
 *
 */
public class Tests extends Fragment {

    @SuppressLint("HandlerLeak")
    private class MessageHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            final Bundle bundle = msg.getData();
            if (bundle.containsKey("error")) {
                Tests.this.logTV.append(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
                Tests.this.appendLog(bundle.getString("who") + " Error: "
                        + bundle.getString("error") + "\n");
            } else if (bundle.containsKey("log")) {
                Tests.this.logTV.append(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
                Tests.this.appendLog(bundle.getString("who") + " "
                        + bundle.getString("log") + "\n");
            }
            Tests.this.performScrollDownOnLog();
        }
    }

    public static final int AT = 6;
    public static final int CURRENT_PLACE = 3;
    public static final int EVENT_PLACE = 5;
    public static final int MOTION_LOG = 9;
    public static final int NEAR = 7;
    public static final int READ = 1;
    public static final int READWRITE = 2;
    public static final int SPATIAL_LOG = 8;
    public static final int START_PLACE = 4;
    public static final int WRITE = 0;

    private static boolean isSpatial(final int st) {
        if (st == Tests.CURRENT_PLACE || st == Tests.START_PLACE
                || st == Tests.EVENT_PLACE || st == Tests.AT
                || st == Tests.NEAR || st == Tests.SPATIAL_LOG
                || st == Tests.MOTION_LOG) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param s
     *            the message to log
     */
    private static void log(final String s) {
        System.out.println("[Tests] " + s);
    }

    private TextView descriptionTV;
    private String ipAddress;
    private String log = "";
    private TextView logTV;
    private MessageHandler messageHandler;
    private String port;
    private View rootView;
    private int selectedTest;
    private String tuplecenter;

    /*****************************************************************************************
     * MESSAGE HANDLER FOR LOGS
     *****************************************************************************************/
    /**
     * @param l
     *            the message to append to the log
     */
    public void appendLog(final String l) {
        this.log = (this.log.equals("") ? "" : this.log) + l;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout
        this.rootView = inflater.inflate(R.layout.fragment_tests, container,
                false);
        this.rootView.findViewById(R.id.frag_tests_startTest)
        .setOnClickListener(new View.OnClickListener() {

                    @Override
            public void onClick(final View view) {
                Tests.this.startTest(Tests.this.selectedTest);
            }
        });
        this.logTV = (TextView) this.rootView.findViewById(R.id.frag_tests_log);
        this.descriptionTV = (TextView) this.rootView
                .findViewById(R.id.frag_tests_description);
        final Spinner spinner = (Spinner) this.rootView
                .findViewById(R.id.tests_spinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(this.getActivity(), R.array.tests_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(final AdapterView<?> parent,
                    final View view, final int pos, final long id) {
                Tests.this.setDescription(pos);
                Tests.this.selectedTest = pos;
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
                /*
                 * Not used atm
                 */
            }
        });
        this.messageHandler = new MessageHandler();
        return this.rootView;
    }

    @Override
    public void onResume() {
        // Read IpAddress to Test from preferences and writing it on View
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        this.ipAddress = sharedPref.getString(
                SettingsFragment.getSettingsTestsIpAddress(), "localhost");
        this.tuplecenter = sharedPref.getString(
                SettingsFragment.getSettingsTestsTupleCentre(), "default");
        this.port = sharedPref.getString(
                SettingsFragment.getSettingsTestsPort(), "20504");
        final TextView view = (TextView) this.rootView
                .findViewById(R.id.frag_tests_header);
        view.setText("Testing on " + this.tuplecenter + "@" + this.ipAddress
                + ":" + this.port);
        this.logTV.setText(this.log);
        this.performScrollDownOnLog();
        super.onResume();
    }

    private boolean geolocationServiceExists() {
        final SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(this.getActivity());
        return sharedPref.getBoolean(
                SettingsFragment.getSettingsGeolocationExist(), false);
    }

    private void performScrollDownOnLog() {
        this.rootView.findViewById(R.id.frag_tests_log_sv).post(new Runnable() {

            @Override
            public void run() {
                ((ScrollView) Tests.this.rootView
                        .findViewById(R.id.frag_tests_log_sv))
                        .fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void postLog(final String s) {
        Tests.log(s);
        this.logTV.append("[Tests] " + s + "\n");
        this.appendLog("[Tests] " + s + "\n");
        this.performScrollDownOnLog();
    }

    private void setDescription(final int pos) {
        switch (pos) {
            case WRITE:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.write_description)));
                break;
            case READ:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.read_description)));
                break;
            case READWRITE:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.readwrite_description)));
                break;
            case CURRENT_PLACE:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.currentplace_description)));
                break;
            case START_PLACE:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.startplace_description)));
                break;
            case EVENT_PLACE:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.eventplace_description)));
                break;
            case AT:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.at_description)));
                break;
            case NEAR:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.near_description)));
                break;
            case SPATIAL_LOG:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.spatial_log_description)));
                break;
            case MOTION_LOG:
                this.descriptionTV.setText(Html.fromHtml(this
                        .getString(R.string.motion_log_description)));
                break;
            default:
                break;
        }
    }

    private void startTest(final int st) {
        if (Tests.isSpatial(st) && !this.geolocationServiceExists()) {
            this.postLog("No geolocation services found. Create one first!");
            Toast.makeText(this.getActivity(),
                    "No geolocation services found. Create one first!",
                    Toast.LENGTH_LONG).show();
        } else {
            final SharedPreferences sharedPref = PreferenceManager
                    .getDefaultSharedPreferences(this.getActivity());
            this.ipAddress = sharedPref.getString(
                    SettingsFragment.getSettingsTestsIpAddress(), "localhost");
            this.tuplecenter = sharedPref.getString(
                    SettingsFragment.getSettingsTestsTupleCentre(), "default");
            this.port = sharedPref.getString(
                    SettingsFragment.getSettingsTestsPort(), "20504");
            if (Tests.isSpatial(st)) {
                final int p = Integer
                        .parseInt(sharedPref.getString(
                                SettingsFragment.getSettingsGeolocationPort(),
                                "20504"));
                final String tuplecentre = sharedPref.getString(
                        SettingsFragment.getSettingsGeolocationTupleCentre(),
                        "default");
                if (!this.tuplecenter.equals(tuplecentre)
                        || !this.port.equals(String.valueOf(p))
                        || !this.ipAddress.equals("localhost")) {
                    this.postLog("The tuple centre name and/or the port specified for tests are not correctly setted. I'm temporarily doing that for you! :)");
                    Toast.makeText(
                            this.getActivity(),
                            "The tuple centre name and/or the port specified for tests are not correctly setted. I'm temporarily doing that for you! :)",
                            Toast.LENGTH_LONG).show();
                    this.tuplecenter = tuplecentre;
                    this.port = String.valueOf(p);
                    this.ipAddress = "localhost";
                }
            }
            try {
                new TestAgent("android_agent", this.ipAddress,
                        this.tuplecenter, this.port, this.messageHandler, st)
                .go();
            } catch (final TucsonInvalidAgentIdException e) {
                this.postLog("Error: " + e.toString());
            }
        }
    }
}
